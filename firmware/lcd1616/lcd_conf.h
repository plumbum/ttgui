#ifndef _LCD_CONF_H_
#define _LCD_CONF_H_

#include "lcd_base.h"

#define LCD_TYPE LCD_TYPE_SPFD54124
// #define LCD_TYPE LCD_TYPE_ILI9325
// #define LCD_TYPE LCD_TYPE_SSD1963

#define LCD_ENABLE_SPIFLASH 0
#define LCD_ENABLE_SD       0

#endif /* _LCD_CONF_H_ */
