/****************************************************************************************************/
/*																									*/
/*	Based on stm32_template by Paul Robson (paul@robsons.org.uk)													*/
/*				Simonsson Fun Techonlogies (original work on code for STM32)						*/
/*																									*/
/****************************************************************************************************/

/* STM32 includes */
#include <stm32f10x.h>
#ifdef USE_STDPERIPH_DRIVER
    #include <stm32f10x_conf.h>
#endif

#include "ttgui.h"
#include "utils.h"

void delay(void);
int main(void);
static void main_noreturn(void) __attribute__((noreturn));

char szBuf[16];
const char hello_world[] = "Hello world!";

#include "logo.hi"

/**
 * Main function
 */
int main(void)
{
	main_noreturn();
}

struct gactor_t act[32];
struct gactor_t* pAct[32];

inline void main_noreturn(void)
{	
	RCC->APB2ENR |= 0x10 | 0x08 | 0x04;									/* Enable the GPIOA (bit 2) and GPIOC (bit 8) */
    GPIOA->ODR = 0xFFFF;
    GPIOA->CRL = 0x88883888;
    GPIOA->CRH = 0x88888888;

    GPIOB->ODR = 0xFFFF;
    GPIOB->CRL = 0x88888338;
    GPIOB->CRH = 0x38338388;

    GPIOC->ODR = 0xFFFF;
	GPIOC->CRH = 0x88888888;
	GPIOC->CRL = 0x88888888;
    
    lcdHwInit();
    lcdHwFillScreen(lcdNightBlue);

    GPIOB->BRR = (1<<1);
    primImage((const uint16_t*)icons_logo_rle, 0, 0);
    GPIOB->BRR = (1<<1) | (1<<2) | (1<<10);

    int i;
    for(i=0; i<32; i++)
        pAct[i] = &act[i];

    gpoint_t pos;
    gpoint_t size;
    i = 0;

    pos.x = 0; pos.y = 28;
    size.x = lcdWidth(); size.y = 16;
    actorInitLabel(pAct[i], NULL, &pos, &size,
            "Test menu");
    pAct[i]->is_disable = true;
    i++;

    size.x = lcdWidth()/2;

    pos.y += size.y;
    actorInitActLabel(pAct[i], NULL, &pos, &size,
            "Action 1", NULL, 0);
    pAct[i]->align = alignRight;
    pAct[i]->is_focused = true;
    i++;

    pos.y += size.y;
    actorInitActLabel(pAct[i], NULL, &pos, &size,
            "Action 2", NULL, 0);
    pAct[i]->align = alignRight;
    i++;

    pos.y += size.y;
    actorInitActLabel(pAct[i], NULL, &pos, &size,
            "Action 3", NULL, 0);
    pAct[i]->align = alignRight;
    i++;

    pos.y += size.y;
    actorInitActLabel(pAct[i], NULL, &pos, &size,
            "Action 4", NULL, 0);
    pAct[i]->align = alignRight;
    i++;

    pos.x = lcdWidth()/2;
    pos.y -= size.y*4;

    pos.y += size.y;
    actorInitActLabel(pAct[i], NULL, &pos, &size,
            "Action 5", NULL, 0);
    pAct[i]->align = alignRight;
    i++;

    pos.y += size.y;
    actorInitActLabel(pAct[i], NULL, &pos, &size,
            "Action 6", NULL, 0);
    pAct[i]->align = alignRight;
    pAct[i]->is_disable = true;
    i++;

    pos.y += size.y;
    actorInitActLabel(pAct[i], NULL, &pos, &size,
            "Action 7", NULL, 0);
    pAct[i]->align = alignRight;
    i++;

    pos.y += size.y;
    actorInitActLabel(pAct[i], NULL, &pos, &size,
            "Action 8", NULL, 0);
    pAct[i]->align = alignRight;
    i++;

    pAct[i] = 0;

    actorRepaint(pAct, gpsSkip);

    uint16_t old_keys = (GPIOA->IDR & (1<<0)) | (GPIOB->IDR & ((1<<4) | (1<<5) | (1<<6) | (1<<7)));
    while(1)
    {
        uint16_t keys = (GPIOA->IDR & (1<<0)) | (GPIOB->IDR & ((1<<4) | (1<<5) | (1<<6) | (1<<7)));
        if(keys != old_keys)
        {
            if(keys & (1<<4))
            {
                actorFocusPrev(pAct);
            }
            if(keys & (1<<6))
            {
                actorFocusNext(pAct);
            }
            old_keys = keys;
            delay_us(50000);
        }

    }

    fontString(fontList[0], 48, 8, "������ ���!", lcdNightBlue, lcdYellow);
    fontString(fontList[0], 48, 24, hello_world, lcdNightBlue, lcdYellow);
    fontString(fontList[0], lcdWidth()-8*(sizeof(hello_world)-1), lcdHeight()-16, hello_world, lcdNightBlue, lcdYellow);


    delay();
    // GPIOA->BRR = (1<<3);

    uint32_t cnt = 0;
    int shift = 0;
    while(1)
    {
        uitoa(cnt, szBuf);
        if((cnt & 0x0F) == 0)
        {
            if(cnt & 0x10)
            {
                primFrame(&defaultStyle, 4, 60, 64, 32, lfRise, 0);
                shift = 0;
            }
            else
            {
                primFrame(&defaultStyle, 4, 60, 64, 32, lfLower, 0);
                shift = 2;
            }
        }
        fontString(fontList[0], 16+shift, 62+shift, szBuf, lcdBlack, lcdLightGray);
        fontString(fontList[1], 24+shift, 78+shift, szBuf, lcdBlack, lcdLightGray);
        cnt++;

        fontString(fontList[0], 96, 14*0, "Button 1", (GPIOA->IDR & (1<<0))?lcdRed:lcdGray, lcdWhite);
        fontString(fontList[0], 96, 14*2, "Button 2", (GPIOB->IDR & (1<<4))?lcdRed:lcdGray, lcdWhite);
        fontString(fontList[0], 96, 14*4, "Button 3", (GPIOB->IDR & (1<<5))?lcdRed:lcdGray, lcdWhite);
        fontString(fontList[0], 96, 14*6, "Button 4", (GPIOB->IDR & (1<<6))?lcdRed:lcdGray, lcdWhite);
        fontString(fontList[0], 96, 14*8, "Button 5", (GPIOB->IDR & (1<<7))?lcdRed:lcdGray, lcdWhite);
    }
}

void delay(void) 
{
	int i = 100000;													/* About 1/4 second delay */
    while (i-- > 0) {
        asm("nop");													/* This stops it optimising code out */
    }
}

