#ifndef _LCD_MCU_H_
#define _LCD_MCU_H_

#include "lcd_mcu_h.h"
#include "stm32f10x.h"

#define LCD_GPIO        GPIOB
#define LCD_PIN_CS      (1<<12)
#define LCD_PIN_SCLK    (1<<13)
#define LCD_PIN_MOSI    (1<<15)

static void lcdMcuDelay(int parrots)
{
    while(parrots--)
    {
        asm volatile (" nop ");
    }
}

static void lcdMcuInit(void)
{
    RCC->APB2ENR |= RCC_APB2ENR_IOPBEN; /* Enable LCD_GPIO */
    LCD_GPIO->BSRR = LCD_PIN_CS;
    LCD_GPIO->BRR = LCD_PIN_SCLK | LCD_PIN_MOSI;
    LCD_GPIO->CRH = (LCD_GPIO->CRL & ~0xFFFF0000) | 0x38330000;
}

static void lcdMcuBusGrab(void)
{
    LCD_GPIO->BRR = LCD_PIN_CS;
}

static void lcdMcuBusRelease(void)
{
    LCD_GPIO->BSRR = LCD_PIN_CS;
}

static void lcdMcuSendWord(lcd_data_t w)
{
    /*
    int i;
    for(i=0; i<sizeof(lcd_data_t)*8; i++)
    {
        if(w & (1<<(sizeof(lcd_data_t)*8-1)))
            LCD_GPIO->BSRR = LCD_PIN_MOSI;
        else
            LCD_GPIO->BRR = LCD_PIN_MOSI;
        LCD_GPIO->BSRR = LCD_PIN_SCLK;
        w <<= 1;
        LCD_GPIO->BRR = LCD_PIN_SCLK;
    }
    */
    if(w & (1<<7)) LCD_GPIO->BSRR = LCD_PIN_MOSI;
              else LCD_GPIO->BRR  = LCD_PIN_MOSI;
    LCD_GPIO->BSRR = LCD_PIN_SCLK; LCD_GPIO->BRR = LCD_PIN_SCLK;
    if(w & (1<<6)) LCD_GPIO->BSRR = LCD_PIN_MOSI;
              else LCD_GPIO->BRR  = LCD_PIN_MOSI;
    LCD_GPIO->BSRR = LCD_PIN_SCLK; LCD_GPIO->BRR = LCD_PIN_SCLK;
    if(w & (1<<5)) LCD_GPIO->BSRR = LCD_PIN_MOSI;
              else LCD_GPIO->BRR  = LCD_PIN_MOSI;
    LCD_GPIO->BSRR = LCD_PIN_SCLK; LCD_GPIO->BRR = LCD_PIN_SCLK;
    if(w & (1<<4)) LCD_GPIO->BSRR = LCD_PIN_MOSI;
              else LCD_GPIO->BRR  = LCD_PIN_MOSI;
    LCD_GPIO->BSRR = LCD_PIN_SCLK; LCD_GPIO->BRR = LCD_PIN_SCLK;
    if(w & (1<<3)) LCD_GPIO->BSRR = LCD_PIN_MOSI;
              else LCD_GPIO->BRR  = LCD_PIN_MOSI;
    LCD_GPIO->BSRR = LCD_PIN_SCLK; LCD_GPIO->BRR = LCD_PIN_SCLK;
    if(w & (1<<2)) LCD_GPIO->BSRR = LCD_PIN_MOSI;
              else LCD_GPIO->BRR  = LCD_PIN_MOSI;
    LCD_GPIO->BSRR = LCD_PIN_SCLK; LCD_GPIO->BRR = LCD_PIN_SCLK;
    if(w & (1<<1)) LCD_GPIO->BSRR = LCD_PIN_MOSI;
              else LCD_GPIO->BRR  = LCD_PIN_MOSI;
    LCD_GPIO->BSRR = LCD_PIN_SCLK; LCD_GPIO->BRR = LCD_PIN_SCLK;
    if(w & (1<<0)) LCD_GPIO->BSRR = LCD_PIN_MOSI;
              else LCD_GPIO->BRR  = LCD_PIN_MOSI;
    LCD_GPIO->BSRR = LCD_PIN_SCLK; LCD_GPIO->BRR = LCD_PIN_SCLK;
}

static void lcdMcuCmd(lcd_data_t cmd)
{
    /* Send address bit: 0 - command */
    LCD_GPIO->BRR = LCD_PIN_MOSI;
    LCD_GPIO->BSRR = LCD_PIN_SCLK;
    LCD_GPIO->BRR = LCD_PIN_SCLK;
    lcdMcuSendWord(cmd);
}

static void lcdMcuData(lcd_data_t data)
{
    /* Send address bit: 1 - command */
    LCD_GPIO->BSRR = LCD_PIN_MOSI;
    LCD_GPIO->BSRR = LCD_PIN_SCLK;
    LCD_GPIO->BRR = LCD_PIN_SCLK;
    lcdMcuSendWord(data);
}

#endif /* _LCD_MCU_H_ */

