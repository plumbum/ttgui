#ifndef _LCD_MCU_H_
#define _LCD_MCU_H_

#include "lcd_mcu_h.h"
#include "stm32f10x.h"

static void lcdMcuDelay(int parrots)
{
    while(parrots--)
    {
        volatile int i;
        for(i=0; i<5000; i++) {}
    }
}

static void lcdMcuInit(void)
{
    GPIOE->BRR = (1<<1);

    RCC->AHBENR |= RCC_AHBENR_FSMCEN;
    //FSMC_Bank1->BTCR[0] = 0;
    //FSMC_Bank1E->BWTR[0] = 0;

    // Register clear
    // Bank1 have NE1 ~ 4, each has a BCR + TCR, so a total of eight registers.
    // Here we use the NE2, also corresponds to BTCR [6], [7].
    FSMC_Bank1->BTCR[2] = 0x00000000;
    FSMC_Bank1->BTCR[3] = 0x00000000;
    FSMC_Bank1E->BWTR[2] = 0x00000000;
    // Register to use asynchronous mode of operation of BCR
    FSMC_Bank1->BTCR[2] |= FSMC_BCR1_WREN; // memory write enable
    FSMC_Bank1->BTCR[2] |= FSMC_BCR1_MWID_0; // memory data width is 16bit
    // Operation BTR register
    FSMC_Bank1->BTCR[3] |= 1<<9; // data retention time for 3 HCLK
    // Flash Write Timing Register
    FSMC_Bank1E->BWTR[2] = 0x0FFFFFFF; // default values
    // Enable BANK4 (PC card Device)
    FSMC_Bank1->BTCR[2] |= FSMC_BCR1_MBKEN;

    lcdMcuDelay(50);
    GPIOE->BSRR = (1<<1);
    lcdMcuDelay(50);
}

static void lcdMcuBusGrab(void)
{
}

static void lcdMcuBusRelease(void)
{
}

#define Bank1_LCD_C    ((uint32_t)0x60000000)	 //display Reg ADDR
#define Bank1_LCD_D    ((uint32_t)0x60020000)    //display Data ADDR

static void lcdMcuCmd(lcd_data_t cmd)
{
    /* Send address bit: 0 - command */
    *(__IO uint16_t *) (Bank1_LCD_C) = cmd;	
}

static void lcdMcuData(lcd_data_t data)
{
    /* Send address bit: 1 - command */
    *(__IO uint16_t *) (Bank1_LCD_D) = data;	
}

static void lcdMcuWriteReg(lcd_data_t reg, lcd_data_t data)
{
    *(__IO uint16_t *) (Bank1_LCD_C) = reg;	
    *(__IO uint16_t *) (Bank1_LCD_D) = data;	
}

static lcd_data_t lcdMcuReadStatus(void)
{
    return *(__IO uint16_t *) (Bank1_LCD_C);
}

static lcd_data_t lcdMcuReadData(void)
{
    return *(__IO uint16_t *) (Bank1_LCD_D);
}

static lcd_data_t lcdMcuReadReg(lcd_data_t reg)
{
    *(__IO uint16_t *) (Bank1_LCD_C) = reg;	
    return *(__IO uint16_t *) (Bank1_LCD_D);
}

#endif /* _LCD_MCU_H_ */

