/* STM32 includes */
#include <stm32f10x.h>
#ifdef USE_STDPERIPH_DRIVER
    #include <stm32f10x_conf.h>
#endif

#include "lcd_hw.h"
#include "lcd_mcu.h"
#include "utils.h"
#include "font.h"
#include "primitives.h"

#include "touch.h"

void delay(void);
int main(void);
static void main_noreturn(void) __attribute__((noreturn));


/*
 * IO pins assignments.
 */
#define GPIOB_LED               5
/*
 * Perif access routines
 */
#define LED_ON() GPIOB->BSRR = (1<<GPIOB_LED)
#define LED_OFF() GPIOB->BRR = (1<<GPIOB_LED)


#include "stepaha.h"
// #include "wine.h"

void putImage(lcd_coords_t x, lcd_coords_t y)
{
    lcd_coords_t xc, yc;
    uint8_t pix[3];
    const char* d = header_data;

    lcdHwStartFrame(x, y, x+width-1, y+height-1);
    for(yc=0; yc<width; yc++)
    {
        for(xc=0; xc<height; xc++)
        {
            HEADER_PIXEL(d, pix);
            lcdHwPutPixel(lcdHwRGB(pix[0], pix[1], pix[2]));
        }
    }
    lcdHwEndFrame();

}


char szBuf[128];
const char hello_world[] = "Hello world!";

/**
 * Main function
 */
int main(void)
{
	main_noreturn();
}

inline void main_noreturn(void)
{	
    RCC->APB2ENR |=
        RCC_APB2ENR_IOPAEN | RCC_APB2ENR_IOPBEN | RCC_APB2ENR_IOPCEN |
        RCC_APB2ENR_IOPDEN | RCC_APB2ENR_IOPEEN | RCC_APB2ENR_IOPFEN |
        RCC_APB2ENR_IOPGEN;
    GPIOA->BSRR = 0xFFFFFFFF;
    GPIOA->CRL = 0xb8b88888;
    GPIOA->CRH = 0x88888888;

    GPIOB->BSRR = 0xFFFFFFFF;
    GPIOB->CRL = 0x38388888;
    GPIOB->CRH = 0x88888888;

    GPIOC->BSRR = 0xFFFFFFFF;
    GPIOC->CRL = 0x88888888;
    GPIOC->CRH = 0x44888888;

    GPIOD->BSRR = 0xFFFFFFFF;
    GPIOD->CRL = 0xb8bb88bb;
    GPIOD->CRH = 0xbb38bbbb;

    GPIOE->BSRR = 0xFFFFFFFF;
    GPIOE->CRL = 0xb8888838;
    GPIOE->CRH = 0xbbbbbbbb;

    GPIOF->BSRR = 0xFFFFFFFF;
    GPIOF->CRL = 0x88888888;
    GPIOF->CRH = 0x88888888;

    GPIOG->BSRR = 0xFFFFFFFF;
    GPIOG->CRL = 0x88888888;
    GPIOG->CRH = 0x88888888;

    /*
    int i;
    while(1)
    {
        LED_ON();
        lcdMcuDelay(500);
        GPIOD->BSRR = (1<<13);
        LED_OFF();
        lcdMcuDelay(500);
        GPIOD->BRR = (1<<13);
    }
    */

    lcdHwInit();
    lcdHwFillScreen(lcdHwRGB(0, 192, 192));

    putImage(0, 0);
    // putImage(100, 48);

    fontString(fontList[0], 48, 8, "������ ���!", lcdNightBlue, lcdYellow);
    fontString(fontList[0], 48, 24, hello_world, lcdNightBlue, lcdYellow);
    fontString(fontList[0], lcdHwWidth()-8*(sizeof(hello_world)-1), lcdHwHeight()-16, hello_world, lcdNightBlue, lcdYellow);


    touchInit();

    uint32_t cnt = 0;
    int shift;
    int x, y;
    int ax, ay;
    ax = 0;
    ay = 0;
    while(1)
    {
        touchGetXY(&x, &y);
        ax += x; ay += y;

        if((cnt & 0x0F) == 0x0F)
        {
            // memset(szBuf, 0, 5);
            uitox((ax/16), szBuf, 4);
            // szBuf[strlen(szBuf)] = ' ';
            // szBuf[5] = 0;
            fontString(fontList[0], 16, 200, szBuf, lcdBlue, lcdYellow);

            // memset(szBuf, 0, 5);
            uitox((ay/16), szBuf, 4);
            // szBuf[strlen(szBuf)] = ' ';
            // szBuf[5] = 0;
            fontString(fontList[0], 16, 214, szBuf, lcdRed, lcdYellow);

            ax = 0; ay = 0;
        }

        uitoa(cnt, szBuf);
        if((cnt & 0xFF) == 0)
        {
            if(cnt & 0x100)
            {
                primFrame(&defaultStyle, 4, 60, 64, 32, lfRise);
                shift = 0;
            }
            else
            {
                primFrame(&defaultStyle, 4, 60, 64, 32, lfLower);
                shift = 2;
            }
        }
        fontString(fontList[0], 16+shift, 62+shift, szBuf, lcdBlack, lcdLightGray);
        fontString(fontList[1], 24+shift, 78+shift, szBuf, lcdBlack, lcdLightGray);
        cnt++;

    }
}

void delay(void) 
{
	int i = 100000;													/* About 1/4 second delay */
while (i-- > 0) {
    asm("nop");													/* This stops it optimising code out */
}
}

