UIDIR = ../ttgui
EXTRAINCDIRS += $(UIDIR)
EXTRAINCDIRS += $(UIDIR)/fonts
EXTRAINCDIRS += $(UIDIR)/icons
SRC += $(UIDIR)/lcd_hw.c
SRC += $(UIDIR)/font.c
SRC += $(UIDIR)/primitives.c

SRC += $(UIDIR)/actors.c

# SRC += $(UIDIR)/touch.c
