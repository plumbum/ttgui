#ifndef _LCD_BASE_H_
#define _LCD_BASE_H_

/* Nokia 1616 LCD */
#define LCD_TYPE_SPFD54124  54124

/* China phone 2.4" - 3.2" LCD */
#define LCD_TYPE_ILI9325    9325

/* 7" display module */
#define LCD_TYPE_SSD1963    1963

#endif /* _LCD_BASE_H_ */

