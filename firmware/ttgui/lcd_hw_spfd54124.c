#include "lcd_hw.h"
#include "lcd_mcu.h"

#define lcdHwOffsetX()    (1)
#define lcdHwOffsetY()    (2)


#define MY (1<<7)
#define MX (1<<6)
#define MV (1<<5)

#define swapcoord(a, b) do { lcd_coords_t _t = a; a = b; b = _t; } while(0)
// #define swapcoord(x, y) do { x = x ^ y; y = x ^ y; x = x ^ y; } while(0)

void lcdHwInit(void)
{
    lcdMcuInit();
    lcdMcuDelay(100);

    lcdMcuBusGrab();
    lcdMcuCmd (0x01); /* Reset display */
    lcdMcuBusRelease();
    lcdMcuDelay(50);

    lcdMcuBusGrab();

    lcdMcuCmd (0xBA); /* Data order */
    lcdMcuData(0x07);
    lcdMcuData(0x15);
    lcdMcuCmd (0x25); /* Contrast */
    lcdMcuData(0x3F);
    lcdMcuCmd (0x11); /* Sleep out */
    lcdMcuCmd (0x13); /* Display normal mode */

    lcdMcuCmd (0x37); /* VSCROLL address */
    lcdMcuData(0x00);
    lcdMcuCmd (0x3A); /* COLMOD pixel format 4=12,5=16,6=18 */
    lcdMcuData(0x05);
    lcdMcuCmd (0x29); /* Display on */
    lcdMcuCmd (0x20); /* INVOFF */
    lcdMcuCmd (0x13); /* Display normal mode */

    lcdMcuCmd (0x36); /* Memory data access control */
    lcdMcuData(MX | MV);

    lcdMcuBusRelease();
}

void lcdHwIdleMode(char on)
{
    lcdMcuBusGrab();
    if(on)
        lcdMcuCmd (0x39); /* Idle mode on */
    else
        lcdMcuCmd (0x38); /* Idle mode off */
    lcdMcuBusRelease();
}

void lcdHwMDA(uint8_t d)
{
    lcdMcuBusGrab();
    lcdMcuCmd (0x36); /* Memory data access control */
    lcdMcuData(d);
    lcdMcuBusRelease();
}


void lcdHwStartFrame(
        lcd_coords_t left, lcd_coords_t top,
        lcd_coords_t right, lcd_coords_t bottom)
{
    lcd_coords_t l, r, t, b;

    l = left;
    r = right;
    t = top;
    b = bottom;

    l += lcdHwOffsetX();
    r += lcdHwOffsetX();
    t += lcdHwOffsetY();
    b += lcdHwOffsetY();

    /*
    if(l > r)
    {
        tmp = r;
        r = l;
        l = tmp;
    }
    if(t > b)
    {
        tmp = b;
        b = t;
        t = tmp;
    }
    */

    lcdMcuBusGrab();
    /* Row frames */
    lcdMcuCmd (0x2A);
    lcdMcuData(l>> 8);
    lcdMcuData(l);
    lcdMcuData(r>> 8);
    lcdMcuData(r);

    /* Column frames */
    lcdMcuCmd (0x2B);
    lcdMcuData(t>> 8);
    lcdMcuData(t);
    lcdMcuData(b>> 8);
    lcdMcuData(b);

    /* Prepare for write */
    lcdMcuCmd (0x2C);
}

void lcdHwEndFrame(void)
{
    lcdMcuBusRelease();
}

void lcdHwPutPixel(lcd_color_t color)
{
    lcdMcuData(color>>8);
    lcdMcuData(color);
}

void lcdHwFillRect(
        lcd_coords_t x1, lcd_coords_t y1,
        lcd_coords_t x2, lcd_coords_t y2,
        lcd_color_t color)
{
    if(x1 > x2) swapcoord(x1, x2);
    if(y1 > y2) swapcoord(y1, y2);

    lcdHwStartFrame(x1, y1, x2, y2);
    int i, j;
    for(j=y1; j<=y2; j++)
    {
        for(i=x1; i<=x2; i++)
        {
            lcdHwPutPixel(color);
        }
    }
    lcdHwEndFrame();
}

void lcdHwFillScreen(lcd_color_t color)
{
    lcdHwStartFrame(0, 0, lcdWidth()-1, lcdHeight()-1);
    int i;
    for(i=0; i<lcdWidth()*lcdHeight(); i++)
    {
        lcdHwPutPixel(color);
    }
    lcdHwEndFrame();
}

void lcdHwPutBitmap(
        lcd_coords_t x, lcd_coords_t y,
        lcd_coords_t w, lcd_coords_t h,
        const uint8_t* data,
        lcd_color_t fg, lcd_color_t bg)
{
    int i, j;
    uint8_t bc = 7;
    uint8_t bits = *data++;
    lcdHwStartFrame(x, y, x+w-1, y+h-1);
    for(j=0; j<h; j++)
    {
        for(i=0; i<w; i++)
        {
            if(bits & 0x80)
                lcdHwPutPixel(fg);
            else
                lcdHwPutPixel(bg);
            if(bc)
            {
                bits <<= 1;
                bc--;
            }
            else
            {
                bits = *data++;
                bc = 7;
            }
        }
    }
    lcdHwEndFrame();
}

#if 0
void lcdHwChar(lcd_coords_t x, lcd_coords_t y, char c, lcd_color_t fg, lcd_color_t bg)
{
    int i, j;
    unsigned char* pC = (unsigned char*)font8x16 + c*16;
    unsigned char bits;
    lcdHwStartFrame(x, y, x+7, y+15);
    for(j=0; j<16; j++)
    {
        bits = *pC++;
        for(i=0; i<8; i++)
        {
            if(bits & 0x80)
                lcdHwPutPixel(fg);
            else
                lcdHwPutPixel(bg);
            bits <<= 1;
        }
    }
    lcdHwEndFrame();
}

void lcdHwChar5(lcd_coords_t x, lcd_coords_t y, char c, lcd_color_t fg, lcd_color_t bg)
{
    char i, j;
    unsigned char* pC = (unsigned char*)font5x8 + c*5;
    unsigned char bits;
    lcdHwMDA(MX);
    lcdHwStartFrame(y, x, y+7, x+5);
    // lcdHwStartFrame(x, y, x+4, y+7);
    for(j=0; j<5; j++)
    {
        bits = *pC++;
        for(i=0; i<8; i++)
        {
            if(bits & 0x01)
                lcdHwPutPixel(fg);
            else
                lcdHwPutPixel(bg);
            bits >>= 1;
        }
    }
    for(i=0; i<8; i++)
    {
        lcdHwPutPixel(bg);
    }
    lcdHwEndFrame();
    lcdHwMDA(MX | MV);
}

void lcdHwPutBitmapVertByte(
        lcd_coords_t x, lcd_coords_t y,
        lcd_coords_t w, lcd_coords_t h8,
        uint8_t* data,
        lcd_color_t fg, lcd_color_t bg)
{
    int i, j;
    uint8_t bc;
    uint8_t bits;
    lcdHwMDA(MX);
    lcdHwStartFrame(y, x, y+(h8*8-1), x+w-1);
    for(j=0; j<w; j++)
    {
        for(i=0; i<h8; i++)
        {
            bits = *data++;
            for(bc=0; bc<8; bc++)
            {
                if(bits & 0x80)
                    lcdHwPutPixel(fg);
                else
                    lcdHwPutPixel(bg);
                bits <<= 1;
            }
        }
    }
    lcdHwEndFrame();
    lcdHwMDA(MX | MV);
}
#endif

