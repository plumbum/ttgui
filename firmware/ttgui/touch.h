#ifndef _TOUCH_H_
#define _TOUCH_H_

#include <inttypes.h>
#include "lcd_hw.h"

typedef enum {
    tsNone = 0,
    tsTouched,
    tsPush,
    tsRelease,
} touch_state_t;

typedef struct {
    lcd_coords_t x;
    lcd_coords_t y;
} gpoint_t;

int touchInit(void);

int touchGetX(void);
int touchGetY(void);
int touchRawX(void);
int touchRawY(void);

int touchGetXY(int* x, int* y);

void touchCalibration(void);

touch_state_t touchWait(gpoint_t* touch, systime_t timeout);

#endif /* _TOUCH_H_ */

