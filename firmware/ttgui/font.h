#ifndef _FONT_H_
#define _FONT_H_

#include <inttypes.h>
#include "lcd_hw.h"

/* Font structure type */
#ifndef FONT_STRUCT_T_DEFINED
#define FONT_STRUCT_T_DEFINED
typedef struct {
    unsigned char width;
    unsigned char height;
    unsigned char space;
    const uint8_t* glyphs;
    const uint16_t* index;
} font_struct_t;
#endif

extern const font_struct_t* fontList[];

void fontChar(const font_struct_t* font,
        lcd_coords_t x, lcd_coords_t y,
        char c,
        lcd_color_t fg, lcd_color_t bg);
void fontString(const font_struct_t* font,
        lcd_coords_t x, lcd_coords_t y,
        const char* str,
        lcd_color_t fg, lcd_color_t bg);

void lputchar(char c);
void lputs(const char* str);

#endif

