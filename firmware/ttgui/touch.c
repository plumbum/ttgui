#include "touch.h"

#include <inttypes.h>
#include <ch.h>
#include <hal.h>
#include "varconf.h"
#include "lcd.h"

#define SPITOUCH SPID1
#define CHX 0x90
#define CHY 0xD0


uint16_t getDisplayCoordinateX(uint16_t x_touch, uint16_t y_touch);
uint16_t getDisplayCoordinateY(uint16_t x_touch, uint16_t y_touch);


/*
 * Low speed SPI configuration (1125KHz, CPHA=0, CPOL=0, MSb first).
 */
static const SPIConfig ls_spicfg = {
    NULL,
    GPIOB,
    GPIOB_TOUCH_CS,
    SPI_CR1_BR_0 | SPI_CR1_BR_1
};


#define RESCALE_FACTOR 1000000

int32_t A2 = 0, B2 = 0, C2 = 0, D2 = 0, E2 = 0, F2 = 0;
uint8_t calibration_done = 0;

char szBuf[16];

int tsRawX;
int tsRawY;

EventSource evtTouchPush;
EventSource evtTouchRelease;
uint8_t state = 0;

int touchGetX(void)
{
    return getDisplayCoordinateX(tsRawX, tsRawY);
}

int touchGetY(void)
{
    return getDisplayCoordinateY(tsRawX, tsRawY);
}

int touchRawX(void)
{
    return tsRawX;
}

int touchRawY(void)
{
    return tsRawY;
}

static WORKING_AREA(touch_thd_wa, 256);
static msg_t touch_thd_ps(void *p)
{
    (void)p;
    uint8_t i;
    int x, y;
    uint8_t idx = 0;
    uint16_t bx[8];
    uint16_t by[8];
    int ax, ay;
    int dx, dy;
    while( ! chThdShouldTerminate())
    {
        if( ! touchGetXY(&x, &y))
        {
            bx[idx] = x;
            by[idx] = y;
            idx = (idx+1) & 7;
            ax = 0; ay = 0;
            for(i=0; i<8; i++)
            {
                ax += bx[i];
                ay += by[i];
            }
            ax /= 8;
            ay /= 8;
            dx = 0; dy = 0;
            for(i=0; i<8; i++)
            {
                int sq;
                sq = (bx[i] - ax);
                sq *= sq; // power 2
                dx += sq;
                sq = (by[i] - ay);
                sq *= sq; // power 2
                dy += sq;
            }
            if((dx < 0x80) && (dy < 0x80))
            {
                tsRawX = x; // getDisplayCoordinateX(x, y);
                tsRawY = y; // getDisplayCoordinateY(x, y);
                if(state == 0)
                {
                    state = 1;
                    chEvtBroadcast(&evtTouchPush);
                }
            }
        }
        else
        {
            if(state == 1)
            {
                state = 0;
                chEvtBroadcast(&evtTouchRelease);
            }
        }
        chThdSleepMilliseconds(7);
    }
    return 0;
}

int touchInit(void)
{
    int ret = 0;
    tsRawX = -1;
    tsRawY = -1;

    chEvtInit(&evtTouchPush);
    chEvtInit(&evtTouchRelease);
    chThdCreateStatic(touch_thd_wa, sizeof(touch_thd_wa),
                      NORMALPRIO-1, touch_thd_ps, NULL);

    if((varRead(VAR_TOUCH_DONE) & 0xFF) != 1)
    {
        touchCalibration();
        ret = 1;
    }
    else
    {
        calibration_done = 1;
    }

    A2 = varRead(VAR_TOUCH_A2);
    B2 = varRead(VAR_TOUCH_B2);
    C2 = varRead(VAR_TOUCH_C2);
    D2 = varRead(VAR_TOUCH_D2);
    E2 = varRead(VAR_TOUCH_E2);
    F2 = varRead(VAR_TOUCH_F2);

    return ret;
}

touch_state_t touchWait(gpoint_t* touch, systime_t timeout)
{
    touch_state_t ts = tsNone;
    EventListener lstPush;
    EventListener lstRelease;
    chEvtRegister(&evtTouchPush, &lstPush, 1);
    chEvtRegister(&evtTouchRelease, &lstRelease, 2);

    chEvtClearFlags(ALL_EVENTS);

    msg_t e = chEvtWaitOneTimeout(EVENT_MASK(1) | EVENT_MASK(2), timeout);
    if(e != 0)
    {
        touch->x = touchGetX();
        touch->y = touchGetY();
        /* Debug. Delete it */
        primLineHoriz(touch->x-1, touch->y, 3, lcdBlack);
        primLineVert(touch->x, touch->y-1, 3, lcdBlack);
        if(e & EVENT_MASK(1))
            ts = tsPush;
        if(e & EVENT_MASK(2))
            ts = tsRelease;
    }
    else
    {
        if(state == 0)
        {
            touch->x = -1;
            touch->y = -1;
        }
        else
        {
            touch->x = touchGetX();
            touch->y = touchGetY();
            ts = tsTouched;
        }
    }
    chEvtUnregister(&evtTouchRelease, &lstRelease);
    chEvtUnregister(&evtTouchPush, &lstPush);
    tsRawX = -1;
    tsRawY = -1;
    return ts;
}

const uint8_t touchRequest[] =
{ CHX, 0, CHX, 0, CHX, 0, CHX, 0, CHY, 0, CHY, 0, CHY, 0, CHY, 0, 0 };

int touchGetXY(int* x, int* y)
{
    uint8_t touchResult[sizeof(touchRequest)];

    spiAcquireBus(&SPITOUCH);              /* Acquire ownership of the bus.    */
    spiStart(&SPITOUCH, &ls_spicfg);       /* Setup transfer parameters.       */
    spiSelect(&SPITOUCH);                  /* Slave Select assertion.          */

    spiExchange(&SPITOUCH, sizeof(touchRequest), touchRequest, touchResult);

    spiUnselect(&SPITOUCH);                /* Slave Select de-assertion.       */
    spiReleaseBus(&SPITOUCH);              /* Ownership release.               */

    int x1=0, y1=0;
    int i;
    for(i=0; i<8; i+=2)
    {
        y1 += (touchResult[i+1]<<8 | touchResult[i+2]);
        x1 += (touchResult[i+9]<<8 | touchResult[i+10]);
    }
    x1 /= 4;
    y1 /= 4;

    if( (y1 < 0x0100) || (y1 > 0x7F00) ||
        (x1 < 0x0100) || (x1 > 0x7F00))
    {
        *x = -1;
        *y = -1;
        return 1;
    }

    *x = x1>>4;
    *y = y1>>4;
    return 0;
}

/*
 * Copy from STM32 GUI Library
 */


/**
  * @brief  Returns the Display y-axis Coordinate corresponding to Touch y-axis coordinate.
  *         X and Y are inverted because the display is meant in Landscape
  * @param  x_touch: X coordinate of the Touch Panel
  * @param  y_touch: Y coordinate of the Touch Panel
  * @retval uint16_t - Xd: Coordinate X of the LCD Panel (X-axis Coordinate)
  */
uint16_t getDisplayCoordinateX(uint16_t x_touch, uint16_t y_touch)
{
    uint16_t Xd;
    float temp;
    temp = (A2 * x_touch + B2 * y_touch + C2) / RESCALE_FACTOR;
    //temp = 800-(float)(x_touch-0x0500)/(0x7300-0x0500)*800;
    Xd = (uint16_t)(temp);
    if (Xd > 60000)
    {
        /* this to avoid negative value */
        Xd = 0;
    }
    return Xd;
}

/**
  * @brief  Returns the Display x-axis Coordinate corresponding to Touch x-axis coordinate.
  *         X and Y are inverted because the display is meant in Landscape
  * @param  x_touch: X coordinate of the Touch Panel
  * @param  y_touch: Y coordinate of the Touch Panel
  * @retval uint16_t - Yd: Coordinate X of the LCD Panel (Y-axis Coordinate)
  */
uint16_t getDisplayCoordinateY(uint16_t x_touch, uint16_t y_touch)
{
    uint16_t Yd;
    float temp;
    temp = (D2 * x_touch + E2 * y_touch + F2) / RESCALE_FACTOR;
    //temp = (float)(y_touch-0x0D00)/(0x6400-0x0D00+200)*480;
    Yd = (uint16_t)(temp);
    if (Yd > 60000)
    {
        /*  this to avoid negative value */
        Yd = 0;
    }
    return Yd;
}


void waitTouch(int* x, int* y)
{
    EventListener lstPush;
    EventListener lstRelease;

    chEvtRegister(&evtTouchPush, &lstPush, 1);
    chEvtRegister(&evtTouchRelease, &lstRelease, 2);

    chEvtWaitOne(EVENT_MASK(1)); // Touch
    *x = tsRawX;
    *y = tsRawY;
    chEvtWaitOne(EVENT_MASK(2)); // Release

    chEvtUnregister(&evtTouchRelease, &lstRelease);
    chEvtUnregister(&evtTouchPush, &lstPush);

}

void drawCross(int y, int x)
{
    lcdHwFillScreen(lcdWhite);
    chThdSleepMilliseconds(300); /* This is to catch only one touch event */
    fontString(fontList[0], 0, 0, "Calibrate touchscreen", lcdNightBlue, lcdYellow);
    primLineHoriz(x-32, y, 64, lcdBlack);
    primLineVert(x, y-32, 64, lcdBlack);
}

void touchCalibration(void)
{
    uint32_t coordinate_X1a = 0, coordinate_X2a = 0, coordinate_X3a = 0, coordinate_X4a = 0, coordinate_X5a = 0;
    uint32_t coordinate_Y1a = 0, coordinate_Y2a = 0, coordinate_Y3a = 0, coordinate_Y4a = 0, coordinate_Y5a = 0;
    uint32_t coordinate_X1b = 0, coordinate_X2b = 0, coordinate_X3b = 0, coordinate_X4b = 0, coordinate_X5b = 0;
    uint32_t coordinate_Y1b = 0, coordinate_Y2b = 0, coordinate_Y3b = 0, coordinate_Y4b = 0, coordinate_Y5b = 0;
    uint32_t coordinate_X1 = 0, coordinate_X2 = 0, coordinate_X3 = 0, coordinate_X4 = 0, coordinate_X5 = 0;
    uint32_t coordinate_Y1 = 0, coordinate_Y2 = 0, coordinate_Y3 = 0, coordinate_Y4 = 0, coordinate_Y5 = 0;
    uint16_t Xd1 = (lcdHwWidth() / 2), Xd2 = 1 * (lcdHwWidth() / 5), Xd3 = 4 * (lcdHwWidth() / 5), Xd4 = 4 * (lcdHwWidth() / 5), Xd5 = 1 * (lcdHwWidth() / 5);
    uint16_t Yd1 = (lcdHwHeight() / 2), Yd2 = 1 * (lcdHwHeight() / 5), Yd3 = 1 * (lcdHwHeight() / 5), Yd4 = 4 * (lcdHwHeight() / 5), Yd5 = 4 * (lcdHwHeight() / 5);
    double A = 0.0, B = 0.0, C = 0.0, D = 0.0, E = 0.0, F = 0.0;
    double d = 0.0, dx1 = 0.0, dx2 = 0.0, dx3 = 0.0, dy1 = 0.0, dy2 = 0.0, dy3 = 0.0;
    uint32_t X2_1 = 0, X2_2 = 0, X2_3 = 0, X2_4 = 0, X2_5 = 0;
    uint32_t Y2_1 = 0, Y2_2 = 0, Y2_3 = 0, Y2_4 = 0, Y2_5 = 0;
    uint32_t XxY_1 = 0, XxY_2 = 0, XxY_3 = 0, XxY_4 = 0, XxY_5 = 0;
    uint32_t XxXd_1 = 0, XxXd_2 = 0, XxXd_3 = 0, XxXd_4 = 0, XxXd_5 = 0;
    uint32_t YxXd_1 = 0, YxXd_2 = 0, YxXd_3 = 0, YxXd_4 = 0, YxXd_5 = 0;
    uint32_t XxYd_1 = 0, XxYd_2 = 0, XxYd_3 = 0, XxYd_4 = 0, XxYd_5 = 0;
    uint32_t YxYd_1 = 0, YxYd_2 = 0, YxYd_3 = 0, YxYd_4 = 0, YxYd_5 = 0;
    uint32_t alfa = 0, beta = 0, chi = 0, Kx = 0, Ky = 0, Lx = 0, Ly = 0;
    uint16_t epsilon = 0, fi = 0, Mx = 0, My = 0;

    int x, y;

    drawCross( (lcdHwHeight() / 2), (lcdHwWidth() / 2) ); /* Absolute Central Point */
    waitTouch(&x, &y);
    coordinate_X1a = x;
    coordinate_Y1a = y;

    drawCross(1*(lcdHwHeight() / 5), 1*(lcdHwWidth() / 5)); /* Nord-East Corner point */
    waitTouch(&x, &y);
    coordinate_X2a = x;
    coordinate_Y2a = y;

    drawCross(1*(lcdHwHeight() / 5), 4*(lcdHwWidth() / 5)); /* Nord-West Corner */
    waitTouch(&x, &y);
    coordinate_X3a = x;
    coordinate_Y3a = y;

    drawCross(4*(lcdHwHeight() / 5), 4*(lcdHwWidth() / 5)); /* Sud-West Corner */
    waitTouch(&x, &y);
    coordinate_X4a = x;
    coordinate_Y4a = y;

    drawCross(4*(lcdHwHeight() / 5), 1*(lcdHwWidth() / 5)); /* Sud-East Corner point */
    waitTouch(&x, &y);
    coordinate_X5a = x;
    coordinate_Y5a = y;

    drawCross( (lcdHwHeight() / 2), (lcdHwWidth() / 2) ); /* Absolute Central Point */
    waitTouch(&x, &y);
    coordinate_X1b = x;
    coordinate_Y1b = y;

    drawCross(1*(lcdHwHeight() / 5), 1*(lcdHwWidth() / 5)); /* Nord-East Corner point */
    waitTouch(&x, &y);
    coordinate_X2b = x;
    coordinate_Y2b = y;

    drawCross(1*(lcdHwHeight() / 5), 4*(lcdHwWidth() / 5)); /* Nord-West Corner */
    waitTouch(&x, &y);
    coordinate_X3b = x;
    coordinate_Y3b = y;

    drawCross(4*(lcdHwHeight() / 5), 4*(lcdHwWidth() / 5)); /* Sud-West Corner */
    waitTouch(&x, &y);
    coordinate_X4b = x;
    coordinate_Y4b = y;

    drawCross(4*(lcdHwHeight() / 5), 1*(lcdHwWidth() / 5)); /* Sud-East Corner point */
    waitTouch(&x, &y);
    coordinate_X5b = x;
    coordinate_Y5b = y;


    /* Average between X and Y coupled Touchscreen values */
    coordinate_X1 = (coordinate_X1a + coordinate_X1b) / 2;
    coordinate_X2 = (coordinate_X2a + coordinate_X2b) / 2;
    coordinate_X3 = (coordinate_X3a + coordinate_X3b) / 2;
    coordinate_X4 = (coordinate_X4a + coordinate_X4b) / 2;
    coordinate_X5 = (coordinate_X5a + coordinate_X5b) / 2;

    coordinate_Y1 = (coordinate_Y1a + coordinate_Y1b) / 2;
    coordinate_Y2 = (coordinate_Y2a + coordinate_Y2b) / 2;
    coordinate_Y3 = (coordinate_Y3a + coordinate_Y3b) / 2;
    coordinate_Y4 = (coordinate_Y4a + coordinate_Y4b) / 2;
    coordinate_Y5 = (coordinate_Y5a + coordinate_Y5b) / 2;

    X2_1 = (coordinate_X1 * coordinate_X1);
    X2_2 = (coordinate_X2 * coordinate_X2);
    X2_3 = (coordinate_X3 * coordinate_X3);
    X2_4 = (coordinate_X4 * coordinate_X4);
    X2_5 = (coordinate_X5 * coordinate_X5);

    Y2_1 = (coordinate_Y1 * coordinate_Y1);
    Y2_2 = (coordinate_Y2 * coordinate_Y2);
    Y2_3 = (coordinate_Y3 * coordinate_Y3);
    Y2_4 = (coordinate_Y4 * coordinate_Y4);
    Y2_5 = (coordinate_Y5 * coordinate_Y5);

    XxY_1 = (coordinate_X1 * coordinate_Y1);
    XxY_2 = (coordinate_X2 * coordinate_Y2);
    XxY_3 = (coordinate_X3 * coordinate_Y3);
    XxY_4 = (coordinate_X4 * coordinate_Y4);
    XxY_5 = (coordinate_X5 * coordinate_Y5);

    XxXd_1 = ( coordinate_X1 * Xd1 );
    XxXd_2 = ( coordinate_X2 * Xd2 );
    XxXd_3 = ( coordinate_X3 * Xd3 );
    XxXd_4 = ( coordinate_X4 * Xd4 );
    XxXd_5 = ( coordinate_X5 * Xd5 );

    YxXd_1 = ( coordinate_Y1 * Xd1 );
    YxXd_2 = ( coordinate_Y2 * Xd2 );
    YxXd_3 = ( coordinate_Y3 * Xd3 );
    YxXd_4 = ( coordinate_Y4 * Xd4 );
    YxXd_5 = ( coordinate_Y5 * Xd5 );

    XxYd_1 = ( coordinate_X1 * Yd1 );
    XxYd_2 = ( coordinate_X2 * Yd2 );
    XxYd_3 = ( coordinate_X3 * Yd3 );
    XxYd_4 = ( coordinate_X4 * Yd4 );
    XxYd_5 = ( coordinate_X5 * Yd5 );

    YxYd_1 = ( coordinate_Y1 * Yd1 );
    YxYd_2 = ( coordinate_Y2 * Yd2 );
    YxYd_3 = ( coordinate_Y3 * Yd3 );
    YxYd_4 = ( coordinate_Y4 * Yd4 );
    YxYd_5 = ( coordinate_Y5 * Yd5 );

    alfa = X2_1 + X2_2 + X2_3 + X2_4 + X2_5;
    beta = Y2_1 + Y2_2 + Y2_3 + Y2_4 + Y2_5;
    chi = XxY_1 + XxY_2 + XxY_3 + XxY_4 + XxY_5;
    epsilon = coordinate_X1 + coordinate_X2 + coordinate_X3 + coordinate_X4 + coordinate_X5;
    fi = coordinate_Y1 + coordinate_Y2 + coordinate_Y3 + coordinate_Y4 + coordinate_Y5;
    Kx = XxXd_1 + XxXd_2 + XxXd_3 + XxXd_4 + XxXd_5;
    Ky = XxYd_1 + XxYd_2 + XxYd_3 + XxYd_4 + XxYd_5;
    Lx = YxXd_1 + YxXd_2 + YxXd_3 + YxXd_4 + YxXd_5;
    Ly = YxYd_1 + YxYd_2 + YxYd_3 + YxYd_4 + YxYd_5;
    Mx = Xd1 + Xd2 + Xd3 + Xd4 + Xd5;
    My = Yd1 + Yd2 + Yd3 + Yd4 + Yd5;

    d = 5 * ( ((double)alfa * beta) - ((double)chi * chi) ) + 2 * ((double)chi * epsilon * fi) - ((double)alfa * fi * fi ) - ( (double)beta * epsilon * epsilon );
    dx1 = 5 * ( ((double)Kx * beta) - ((double)Lx * chi) ) + ((double)fi * ( ((double)Lx * epsilon) - ((double)Kx * fi) )) + ((double)Mx * ( ((double)chi * fi) - ((double)beta * epsilon) ));
    dx2 = 5 * ( ((double)Lx * alfa) - ((double)Kx * chi) ) + ((double)epsilon * ( ((double)Kx * fi) - ((double)Lx * epsilon) )) + ((double)Mx * ( ((double)chi * epsilon) - ((double)alfa * fi) ));
    dx3 = ((double)Kx * ( ((double)chi * fi) - ((double)beta * epsilon) )) + ((double)Lx * ( ((double)chi * epsilon) - ((double)alfa * fi) )) + ((double)Mx * ( ((double)alfa * beta) - ((double)chi * chi) ));
    dy1 = 5 * ( ((double)Ky * beta) - ((double)Ly * chi) ) + ((double)fi * ( ((double)Ly * epsilon) - ((double)Ky * fi) )) + ((double)My * ( ((double)chi * fi) - ((double)beta * epsilon) ));
    dy2 = 5 * ( ((double)Ly * alfa) - ((double)Ky * chi) ) + ((double)epsilon * ( ((double)Ky * fi) - ((double)Ly * epsilon) )) + ((double)My * ( ((double)chi * epsilon) - ((double)alfa * fi) ));
    dy3 = ((double)Ky * ( ((double)chi * fi) - ((double)beta * epsilon) )) + ((double)Ly * ( ((double)chi * epsilon) - ((double)alfa * fi) )) + ((double)My * ( ((double)alfa * beta) - ((double)chi * chi) ));

    A = dx1 / d;
    B = dx2 / d;
    C = dx3 / d;
    D = dy1 / d;
    E = dy2 / d;
    F = dy3 / d;

    /* To avoid computation with "double" variables A, B, C, D, E, F, we use the s32 variables
       A2, B2, C2, D2, E2, F2, multiplied for a Scale Factor equal to 100000 to retain the precision*/
    A2 = (int32_t)(A * RESCALE_FACTOR);
    B2 = (int32_t)(B * RESCALE_FACTOR);
    C2 = (int32_t)(C * RESCALE_FACTOR);
    D2 = (int32_t)(D * RESCALE_FACTOR);
    E2 = (int32_t)(E * RESCALE_FACTOR);
    F2 = (int32_t)(F * RESCALE_FACTOR);


    lcdHwFillScreen(lcdWhite);
    fontString(fontList[0], 0, 0, "Calibration done!", lcdNightBlue, lcdYellow);

    calibration_done = 1;

    varWrite(VAR_TOUCH_A2, A2);
    varWrite(VAR_TOUCH_B2, B2);
    varWrite(VAR_TOUCH_C2, C2);
    varWrite(VAR_TOUCH_D2, D2);
    varWrite(VAR_TOUCH_E2, E2);
    varWrite(VAR_TOUCH_F2, F2);
    varWrite(VAR_TOUCH_DONE, 1);
    varCommit();
}

