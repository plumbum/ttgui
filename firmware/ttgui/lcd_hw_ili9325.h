#ifndef _LCD_ILI9325_H_
#define _LCD_ILI9325_H_

#include <inttypes.h>

#define LCD_CONTROLLER_STRING "ILI9325"
#define LCD_GLASS_STRING "320x240 glasses"
#define LCD_INTERFACE_STRING "16bit parallel"

typedef uint16_t lcd_data_t;
typedef int16_t lcd_coords_t;
typedef uint16_t lcd_color_t;

#define lcdWidth()      (320)
#define lcdHeight()     (240)

#endif /* _LCD_ILI9325_H_ */

