#include "lcd_hw.h"
#include "lcd_mcu.h"

#define lcdHwOffsetX()    (0)
#define lcdHwOffsetY()    (0)

#define LCD_GLASS 0

#define swapcoord(a, b) do { lcd_coords_t _t = a; a = b; b = _t; } while(0)

#if (LCD_GLASS == 1)

unsigned int  HDP=799; //Horizontal Display Period
unsigned int  HT=1000; //Horizontal Total (1000)
unsigned int  HPS=51;  //LLINE Pulse Start Position
unsigned int  LPS=3;   //	Horizontal Display Period Start Position
unsigned char HPW=8;   //	LLINE Pulse Width (8)

unsigned int  VDP=479;	//Vertical Display Period
unsigned int  VT=530;	//Vertical Total
unsigned int  VPS=24;	//	LFRAME Pulse Start Position (24)
unsigned int  FPS=23;	//Vertical Display Period Start Position (23)
unsigned char   VPW=3;	// LFRAME Pulse Width

#else

unsigned int  HDP=799; //Horizontal Display Period
unsigned int  HT=850; //Horizontal Total (1000)
unsigned int  HPS=51;  //LLINE Pulse Start Position
unsigned int  LPS=3;   //	Horizontal Display Period Start Position
unsigned char HPW=48;   //	LLINE Pulse Width (8)

unsigned int  VDP=479;	//Vertical Display Period
unsigned int  VT=530;	//Vertical Total
unsigned int  VPS=50;	//	LFRAME Pulse Start Position (24)
unsigned int  FPS=30;	//Vertical Display Period Start Position (23)
unsigned char   VPW=3;	// LFRAME Pulse Width
#endif


void lcdHwInit(void)
{
    lcdMcuInit();
    lcdMcuDelay(100);

#if (LCD_TYPE_SSD1963 == 1)
	lcdMcuCmd(0x00E2);	
	lcdMcuData(0x0023);
	// Set PLL with OSC = 10MHz (hardware)
    // Multiplier N = 35, VCO (>250MHz)= OSC*(N+1), VCO = 360MHz	   
	lcdMcuData(0x0002);
	// Divider M = 2, PLL = 360/(M+1) = 120MHz
	lcdMcuData(0x0004);
	// Validate M and N values

	lcdMcuCmd(0x00E0);  // PLL enable
	lcdMcuData(0x0001);
	lcdMcuDelay(1);
	lcdMcuCmd(0x00E0);
	lcdMcuData(0x0003);
	lcdMcuDelay(5);
	lcdMcuCmd(0x0001);  // software reset
	lcdMcuDelay(5);
	lcdMcuCmd(0x00E6);	//PLL setting for PCLK, depends on resolution
	//Set LSHIFT freq, i.e. the DCLK with PLL freq 120MHz set previously
	//Typical DCLK for AT070TN92 is 34MHz
	//34MHz = 120MHz*(LCDC_FPR+1)/2^20
	//LCDC_FPR = 300000 (0x0493E0)
	
	
#if (LCD_GLASS == 1)
	lcdMcuData(0x0004);
	lcdMcuData(0x0093);
	lcdMcuData(0x00e0);
    /*
    lcdMcuData(0x0000);
	lcdMcuData(0x00b4);
	lcdMcuData(0x00e7);
    */
#else
	lcdMcuData(0x0003);
	lcdMcuData(0x0033);
	lcdMcuData(0x0033);
#endif


	lcdMcuCmd(0x00B0);	//LCD SPECIFICATION
	lcdMcuData(0x0000);
	lcdMcuData(0x0000);
	lcdMcuData((HDP>>8)&0X00FF);  //Set HDP
	lcdMcuData(HDP&0X00FF);
    lcdMcuData((VDP>>8)&0X00FF);  //Set VDP
	lcdMcuData(VDP&0X00FF);
    lcdMcuData(0x0000);

	lcdMcuCmd(0x00B4);	//HSYNC
	lcdMcuData((HT>>8)&0X00FF);  //Set HT
	lcdMcuData(HT&0X00FF);
	lcdMcuData((HPS>>8)&0X00FF);  //Set HPS
	lcdMcuData(HPS&0X00FF);
	lcdMcuData(HPW);			   //Set HPW
	lcdMcuData((LPS>>8)&0X00FF);  //Set HPS
	lcdMcuData(LPS&0X00FF);
	lcdMcuData(0x0000);

	lcdMcuCmd(0x00B6);	//VSYNC
	lcdMcuData((VT>>8)&0X00FF);   //Set VT
	lcdMcuData(VT&0X00FF);
	lcdMcuData((VPS>>8)&0X00FF);  //Set VPS
	lcdMcuData(VPS&0X00FF);
	lcdMcuData(VPW);			   //Set VPW
	lcdMcuData((FPS>>8)&0X00FF);  //Set FPS
	lcdMcuData(FPS&0X00FF);

	lcdMcuCmd(0x00BA);
	lcdMcuData(0x000F);    //GPIO[3:0] out 1

	lcdMcuCmd(0x00B8);
	lcdMcuData(0x0007);    //GPIO3=input, GPIO[2:0]=output
	lcdMcuData(0x0001);    //GPIO0 normal

	lcdMcuCmd(0x0036); //rotation
#if (LCD_GLASS == 1)
	lcdMcuData(0x0000);
#else
	lcdMcuData(0x0001); // (0)
#endif
	// lcdMcuData(0x0060);


	lcdMcuCmd(0x00F0); //pixel data interface
	lcdMcuData(0x0003);


	lcdMcuDelay(5);

	//LCD_clear(0xf800);
	//TFT_CLEAR(0xf800);
	
	lcdMcuCmd(0x0026); //display on
	lcdMcuData(0x0001);

	lcdMcuCmd(0x0029); //display on

	lcdMcuCmd(0x00BE); //set PWM for B/L
#if (LCD_GLASS == 1)
	lcdMcuData(0x0004);
	lcdMcuData(0x0080);
#else
	lcdMcuData(0x0006);
	lcdMcuData(0x0080);
#endif
	
	lcdMcuData(0x0001);
	lcdMcuData(0x00f0);
	lcdMcuData(0x0000);
	lcdMcuData(0x0000);

	lcdMcuCmd(0x00d0);//ЙиЦГ¶ЇМ¬±і№вїШЦЖЕдЦГ 
	lcdMcuData(0x000d);

#else
#error "Unsupported chip"
#endif
}



void lcdHwIdleMode(char on)
{
}

void lcdHwMDA(uint8_t d)
{
}


void lcdHwStartFrame(
        lcd_coords_t left, lcd_coords_t top,
        lcd_coords_t right, lcd_coords_t bottom)
{
    lcd_coords_t l, r, t, b;

    l = left;
    r = right;
    t = top;
    b = bottom;

    lcdMcuBusGrab();
    /* Column frames */
    lcdMcuCmd(0x2A);
    lcdMcuData(l >> 8);
    lcdMcuData(l & 0xFF);
    lcdMcuData(r >> 8);
    lcdMcuData(r & 0xFF);

    /* Row frames */
    lcdMcuCmd(0x2B);
    lcdMcuData(t >> 8);
    lcdMcuData(t & 0xFF);
    lcdMcuData(b >> 8);
    lcdMcuData(b & 0xFF);

    /* Prepare for write */
    lcdMcuCmd(0x2C);
}

void lcdHwEndFrame(void)
{
    lcdMcuBusRelease();
}

void lcdHwPutPixel(lcd_color_t color)
{
    lcdMcuData(color);
}

void lcdHwFillRect(
        lcd_coords_t x1, lcd_coords_t y1,
        lcd_coords_t x2, lcd_coords_t y2,
        lcd_color_t color)
{
    if(x1 > x2) swapcoord(x1, x2);
    if(y1 > y2) swapcoord(y1, y2);

    lcdHwStartFrame(x1, y1, x2, y2);
    int i, j;
    for(j=y1; j<=y2; j++)
    {
        for(i=x1; i<=x2; i++)
        {
            lcdHwPutPixel(color);
        }
    }
    lcdHwEndFrame();
}

void lcdHwFillScreen(lcd_color_t color)
{
    lcdHwStartFrame(0, 0, lcdHwWidth()-1, lcdHwHeight()-1);
    lcdMcuFillBlock(color, lcdHwWidth()*lcdHwHeight());
    lcdHwEndFrame();
}

void lcdHwPutBitmap(
        lcd_coords_t x, lcd_coords_t y,
        lcd_coords_t w, lcd_coords_t h,
        const uint8_t* data,
        lcd_color_t fg, lcd_color_t bg)
{
    int i, j;
    uint8_t bc = 7;
    uint8_t bits = *data++;
    lcdHwStartFrame(x, y, x+w-1, y+h-1);
    for(j=0; j<h; j++)
    {
        for(i=0; i<w; i++)
        {
            if(bits & 0x80)
                lcdHwPutPixel(fg);
            else
                lcdHwPutPixel(bg);
            if(bc)
            {
                bits <<= 1;
                bc--;
            }
            else
            {
                bits = *data++;
                bc = 7;
            }
        }
    }
    lcdHwEndFrame();
}

