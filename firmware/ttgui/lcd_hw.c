#include "lcd_hw.h"
#include "lcd_mcu.h"

#if (LCD_TYPE == LCD_TYPE_SPFD54124)
#   include "lcd_hw_spfd54124.c"
#elif (LCD_TYPE == LCD_TYPE_ILI9325)
#   include "lcd_hw_ili9325.c"
#elif (LCD_TYPE == LCD_TYPE_SSD1963)
#   include "lcd_hw_ssd1963.c"
#else
#error "Unknow LCD_TYPE_x"
#endif

