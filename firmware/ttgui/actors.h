#ifndef _ACTORS_H_
#define _ACTORS_H_

#include "lcd_hw.h"
#include "primitives.h"
// #include "touch.h"

#define ID_NUM_0        0x500
#define ID_NUM_1        0x501
#define ID_NUM_2        0x502
#define ID_NUM_3        0x503
#define ID_NUM_4        0x504
#define ID_NUM_5        0x505
#define ID_NUM_6        0x506
#define ID_NUM_7        0x507
#define ID_NUM_8        0x508
#define ID_NUM_9        0x509
#define ID_NUM_BS       0x50A
#define ID_NUM_CLEAR    0x50B
#define ID_NUM_OK       0x50C
#define ID_NUM_CANCEL   0x50D

#ifndef false
#define false (0)
#endif

#ifndef true
#define true (~false)
#endif

#ifndef NULL
#define NULL ((void*)0)
#endif

struct gactor_t;

typedef struct {
    lcd_coords_t x;
    lcd_coords_t y;
} gpoint_t;

typedef struct {
    gpoint_t pos;
    gpoint_t size;
} grect_t;

typedef enum {
    gpsSkip = -1,
    gpsNone = 0,
    gpsDown,
    gpsPush,
    gpsRelease,
} gpush_state_t;


typedef void (gpaint_f)(struct gactor_t* actor, gpush_state_t state);
typedef int (gaction_f)(struct gactor_t* actor, gpush_state_t state);

typedef enum {
    alignLeft,
    alignCenter,
    alignRight,
    alignNone,
} align_t;

typedef enum {
    valignTop,
    valignCenter,
    valignBottom,
    valignNone,
} valign_t;

struct gactor_t {
    /* Constant items */
    int actor_id;
    struct gactor_t* parent;
    gpoint_t position;
    gpoint_t size;
    const lcd_style_t* gstyle;
    lcd_frame_t frame;

    gpaint_f* paint_call;
    gaction_f* action_call;

    lcd_image_t img;
    const char* text;
    align_t align;
    valign_t valign;

    gpoint_t padding;

    int is_static:1;
    int is_hide:1;
    int is_disable:1;
    int is_checkable:1;
    int is_checked:1;
    int is_focused:1;
    int need_repaint:1;
};


/**
 * Init generic actor
 * @param *actor - pointer object description structure
 * @param *parent - parent object
 * @param *pos - left-top object corner 
 * @param *size - width-height object rect
 */
void actorInit(struct gactor_t* actor, struct gactor_t* parent, gpoint_t* pos, gpoint_t* size);

/* Object specified initializators */
void actorInitFrame(struct gactor_t* actor, struct gactor_t* parent, gpoint_t* pos, gpoint_t* size);

void actorInitImage(struct gactor_t* actor, struct gactor_t* parent, gpoint_t* pos, gpoint_t* size, lcd_image_t img);

void actorInitButton(struct gactor_t* actor, struct gactor_t* parent, gpoint_t* pos, gpoint_t* size, const char* text, gaction_f* touch, int actor_id);

void actorInitImgButton(struct gactor_t* actor, struct gactor_t* parent, gpoint_t* pos, gpoint_t* size, lcd_image_t img, const char* text, gaction_f* touch, int actor_id);

void actorInitLabel(struct gactor_t* actor, struct gactor_t* parent, gpoint_t* pos, gpoint_t* size, const char* text);

void actorInitActLabel(struct gactor_t* actor, struct gactor_t* parent, gpoint_t* pos, gpoint_t* size, const char* text, gaction_f* touch, int actor_id);


/* Paint objects */
void actorPaintFrame(struct gactor_t* actor, gpush_state_t ts);
void actorPaintButton(struct gactor_t* actor, gpush_state_t ts);
void actorPaintLabel(struct gactor_t* actor, gpush_state_t ts);
void actorPaintImage(struct gactor_t* actor, gpush_state_t ts);

/**
 * Process action for single actor
 * @return 0 if action not processed
 */
int actorAction(struct gactor_t* actor, gpush_state_t ts);

/**
 * Process action list (NULL ended) and call focused actor
 */
struct gactor_t* actorActionList(struct gactor_t** actor_list, gpush_state_t ts);

/**
 * Process action list (NULL ended) and call actor at touch_coord
 */
struct gactor_t* actorActionListTouch(struct gactor_t** actor_list, gpoint_t* touch_coord, gpush_state_t ts);

/**
 * Paint single actor
 */
void actorPaint(struct gactor_t* actor, gpush_state_t ts);

/**
 * Repaint actors marked as 'need_repaint'
 */
void actorPaintList(struct gactor_t** actor_list, gpush_state_t ts);

/**
 * Repaint all actors in NULL ended list
 */
void actorRepaint(struct gactor_t** actor_list, gpush_state_t ts);

/**
 * Align actor position at center of parent actor
 * If actor have no parent align by display bounds.
 */
void actorCenter(struct gactor_t* actor);

/**
 * Align actor position reference by parent actor
 * If actor have no parent align by display bounds.
 */
void actorAlign(struct gactor_t* actor, align_t align, valign_t valign);

/**
 * Set focus by actor pointer.
 * @return focused actor_id
 */
int actorSetFocus(struct gactor_t** actor_list, struct gactor_t* actor);

/**
 * Set focus by actor pointer.
 * @return focused *actor
 */
struct gactor_t* actorSetFocusById(struct gactor_t** actor_list, int id);

/**
 * @return focused actor
 */
struct gactor_t* actorGetFocus(struct gactor_t** actor_list);

/**
 * @return focused actor_id
 */
int actorGetFocusId(struct gactor_t** actor_list);

/**
 * @return *actor
 */
struct gactor_t* actorGetById(struct gactor_t** actor_list, int id);


/**
 * Rotation to next active actor
 * @return focused actor
 */
struct gactor_t* actorFocusNext(struct gactor_t** actor_list);

/**
 * Rotation to previous active actor
 * @return focused actor
 */
struct gactor_t* actorFocusPrev(struct gactor_t** actor_list);

#endif /* _ACTORS_H_ */

