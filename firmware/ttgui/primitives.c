#include "primitives.h"

#if (LCD_ENABLE_SPIFLASH != 0)
#include "spiflash.h"
#endif

/*
 *
 * #FFFFFF 
 * #DFDFDF
 * #C0C0C0 filling
 * #808080
 * #000000
 */

const lcd_style_t defaultStyle = {
    lcdBlack,           /* text */
    lcdLightGray,       /* frame bg */
    lcdNightBlue,       /* bg */
    lcdWhite,           /* rise 1 */
    lcdHwRGB(0xDF, 0xDF, 0xDF), /* rise 2 */
    lcdGray,            /* fade 1 */
    lcdBlack,           /* fade 2 */
    lcdDarkGray,        /* gray text */
    lcdWhite,           /* selected text */
    lcdHwRGB(0, 0, 0x80), /* selected background */
    1
};

void primFillRect(lcd_coords_t x, lcd_coords_t y,
        lcd_coords_t w, lcd_coords_t h,
        lcd_color_t color)
{
    lcdHwFillRect(x, y, x+w-1, y+h-1, color);
}

void primLineHoriz(lcd_coords_t x, lcd_coords_t y,
        lcd_coords_t w,
        lcd_color_t color)
{
    lcdHwFillRect(x, y, x+w-1, y, color);
}

void primLineVert(lcd_coords_t x, lcd_coords_t y,
        lcd_coords_t h,
        lcd_color_t color)
{
    lcdHwFillRect(x, y, x, y+h-1, color);
}

void primFrame(const lcd_style_t* style, lcd_coords_t x, lcd_coords_t y,
        lcd_coords_t w, lcd_coords_t h,
        lcd_frame_t type, int selected)
{
    lcd_coords_t x2, y2;
    x2 = x+w-1;
    y2 = y+h-1;
    switch(type)
    {
        case lfNone:
            if(selected)
                lcdHwFillRect(x, y, x2, y2, style->sel_bg);
            else
                lcdHwFillRect(x, y, x2, y2, style->frame_bg);
            break;
        case lfFlat:
            if(selected)
                lcdHwFillRect(x+1, y+1, x2-1, y2-1, style->sel_bg);
            else
                lcdHwFillRect(x+1, y+1, x2-1, y2-1, style->frame_bg);
            lcdHwFillRect(x, y, x2, y, style->fade1);
            lcdHwFillRect(x, y2, x2, y2, style->fade1);
            lcdHwFillRect(x, y+1, x, y2-1, style->fade1);
            lcdHwFillRect(x2, y+1, x2, y2-1, style->fade1);
            break;
        default:
            do {
                lcd_coords_t rise1, rise2, fade1, fade2;
                if(type == lfRise)
                {
                    rise1 = style->rise1;
                    rise2 = style->rise2;
                    fade1 = style->fade1;
                    fade2 = style->fade2;
                }
                else
                {
                    rise1 = style->fade2;
                    rise2 = style->fade1;
                    fade1 = style->rise2;
                    fade2 = style->rise1;
                }

                if(selected)
                    lcdHwFillRect(x+2, y+2, x2-2, y2-2, style->sel_bg);
                else
                    lcdHwFillRect(x+2, y+2, x2-2, y2-2, style->frame_bg);

                lcdHwFillRect(x, y, x2-1, y, rise1);
                lcdHwFillRect(x, y+1, x, y2-1, rise1);

                lcdHwFillRect(x+1, y+1, x2-2, y+1, rise2);
                lcdHwFillRect(x+1, y+2, x+1, y2-2, rise2);

                lcdHwFillRect(x2-1, y+1, x2-1, y2-2, fade1);
                lcdHwFillRect(x+1, y2-1, x2-1, y2-1, fade1);

                lcdHwFillRect(x2, y, x2, y2-1, fade2);
                lcdHwFillRect(x, y2, x2, y2, fade2);
            } while(0);
            break;
    }
}

void primText(const lcd_style_t* style,
        lcd_coords_t x, lcd_coords_t y,
        const char* text, int flags)
{
    lcd_color_t fg, bg;
    if(flags & PRIM_FLAG_SELECTED)
    {
        fg = style->sel_text;
        bg = style->sel_bg;
    }
    else
    {
        if(flags & PRIM_FLAG_DISABLED)
            fg = style->gray_text;
        else
            fg = style->text;
        bg = style->frame_bg;
    }
    fontString(fontList[style->font_idx], x, y,
        text, fg, bg);
}


#if (LCD_ENABLE_SPIFLASH != 0)

#define IMG_BLOCK_SIZE 128
static uint16_t img_buf[IMG_BLOCK_SIZE];

#define NEXT_BLOCK_WORD() do { \
    block_cnt--; \
    if(!block_cnt) \
    { \
        sfReadBlock((uint8_t*)img_buf, sf_addr, IMG_BLOCK_SIZE*2); \
        sf_addr += IMG_BLOCK_SIZE*2; \
        block_cnt = IMG_BLOCK_SIZE; \
        pib = img_buf; \
    } \
} while(0)

void primSfImageSize(const uint16_t* sf_addr, lcd_coords_t* width, lcd_coords_t* height)
{
    uint16_t buf[2];
    sfReadBlock((uint8_t*)buf, sf_addr, 4);
    *width = buf[0];
    *height = buf[1];
}

void primSfImage(const uint16_t* sf_addr, lcd_coords_t x, lcd_coords_t y)
{
    int block_cnt = IMG_BLOCK_SIZE;
    uint16_t* pib = img_buf;
    int i;
    uint16_t width, height;
    uint16_t lb;
    lcd_color_t color;

    sfReadBlock((uint8_t*)img_buf, sf_addr, IMG_BLOCK_SIZE*2);
    sf_addr += IMG_BLOCK_SIZE*2;

    width = *pib++;
    height = *pib++;
    block_cnt -= 2;

    if(width > lcdHwWidth()) return;
    if(height > lcdHwHeight()) return;

    lcdHwStartFrame(x, y, x+width-1, y+height-1);
    i = width*height;
    while(i > 0)
    {
        lb = *pib++;
        NEXT_BLOCK_WORD();
        if(lb & 0x8000)
        {
            color = *pib++;
            NEXT_BLOCK_WORD();
            lb &= 0x7FFF;
            i -= lb;
            while(lb--)
            {
                lcdHwPutPixel(color);
            }
        }
        else
        {
            i -= lb;
            while(lb--)
            {
                color = *pib++;
                NEXT_BLOCK_WORD();
                lcdHwPutPixel(color);
            }
        }
    }
    lcdHwEndFrame();
}
#endif

void primImage(const uint16_t* addr, lcd_coords_t x, lcd_coords_t y)
{
    int i;
    uint16_t width, height;
    uint16_t lb;
    lcd_color_t color;

    width = *addr++;
    height = *addr++;
    lcdHwStartFrame(x, y, x+width-1, y+height-1);
    i = width*height;
    while(i > 0)
    {
        lb = *addr++;
        if(lb & 0x8000)
        {
            color = *addr++;
            lb &= 0x7FFF;
            i -= lb;
            while(lb--)
            {
                lcdHwPutPixel(color);
            }
        }
        else
        {
            i -= lb;
            while(lb--)
            {
                color = *addr++;
                lcdHwPutPixel(color);
            }
        }
    }
    lcdHwEndFrame();
}

void primImageSize(const uint16_t* addr, lcd_coords_t* width, lcd_coords_t* height)
{
    *width = *addr++;
    *height = *addr++;
}

/* TODO
 * circle
 * fill circle
 * arc
 * line
 * fill area
 */

