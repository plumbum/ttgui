#include "actors.h"

#include <string.h>

#include "lcd.h"

#include "check_bg.hi"
#include "cross_bg.hi"

#ifndef DEFAULT_PADDING_X
#define DEFAULT_PADDING_X 4
#endif

#ifndef DEFAULT_PADDING_Y
#define DEFAULT_PADDING_Y 4
#endif

int checkPoint(gpoint_t* pnt, struct gactor_t* actor)
{
    if(pnt->x < actor->position.x) return 0;
    if(pnt->x >= actor->position.x+actor->size.x) return 0;
    if(pnt->y < actor->position.y) return 0;
    if(pnt->y >= actor->position.y+actor->size.y) return 0;
    return 1;
}

int actorAction(struct gactor_t* actor, gpush_state_t ts)
{
    int r = 0;
    int repaint = false;
    if((ts == gpsPush) && (actor->is_checkable))
    {
        actor->is_checked = !actor->is_checked;
        repaint = true;
    }
    if( actor->action_call != NULL )
    {
        r = actor->action_call(actor, ts);
        repaint = true;
    }
    if( repaint && (actor->paint_call != NULL) )
        actor->paint_call(actor, ts);
    return r;
}

struct gactor_t* actorActionList(struct gactor_t** actor_list, gpush_state_t ts)
{
    struct gactor_t** p = actor_list;
    while(*p != NULL)
    {
        struct gactor_t* actor = *p;
        if( actor->is_static == 0 )
        {
            if( actor->is_focused )
            {
                if( actorAction(actor, ts) )
                {
                    return actor;
                }
            }
        }
        p++;
    }
    return NULL;
}

struct gactor_t* actorActionListTouch(struct gactor_t** actor_list, gpoint_t* touch_coord, gpush_state_t ts)
{
    struct gactor_t** p = actor_list;
    while(*p != NULL)
    {
        struct gactor_t* actor = *p;
        if( actor->is_static == 0 )
        {
            if( checkPoint(touch_coord, actor) )
            {
                if( actorAction(actor, ts) )
                {
                    return actor;
                }
            }
        }
        p++;
    }
    return NULL;
}

void actorPaint(struct gactor_t* actor, gpush_state_t ts)
{
    if( actor->paint_call != NULL )
        actor->paint_call(actor, ts);
}

void actorPaintList(struct gactor_t** actor_list, gpush_state_t ts)
{
    struct gactor_t** p = actor_list;
    while(*p != NULL)
    {
        struct gactor_t* actor = *p;
        if(actor->need_repaint)
        {
            actorPaint(*p, ts);
            actor->need_repaint = false;
        }
        p++;
    }
}

void actorRepaint(struct gactor_t** actor_list, gpush_state_t ts)
{
    struct gactor_t** p = actor_list;
    while(*p != NULL)
    {
        actorPaint(*p, ts);
        (*p)->need_repaint = false;
        p++;
    }
}

void actorInit(struct gactor_t* actor, struct gactor_t* parent, gpoint_t* pos, gpoint_t* size)
{
    actor->parent = parent;

    actor->position = *pos;
    actor->size = *size;

    if(parent != NULL)
    {
        actor->position.x += parent->position.x;
        actor->position.y += parent->position.y;
        actor->gstyle = parent->gstyle;
    }
    else
    {
        actor->gstyle = (lcd_style_t*)&defaultStyle;
    }

    actor->paint_call = NULL;
    actor->action_call = NULL;

    actor->text = NULL;
    actor->align = alignCenter;
    actor->valign = alignCenter;
    actor->padding.x = DEFAULT_PADDING_X;
    actor->padding.y = DEFAULT_PADDING_Y;
    actor->img.storage = imgNone;
    actor->is_static = true;
    actor->is_checked = false;
    actor->is_focused = false;
    actor->need_repaint = true;
    actor->actor_id = 0;
}

void actorInitFrame(struct gactor_t* actor, struct gactor_t* parent, gpoint_t* pos, gpoint_t* size)
{
    actorInit(actor, parent, pos, size);
    actor->paint_call = actorPaintFrame;
    actor->frame = lfFlat;
}

void actorInitImage(struct gactor_t* actor, struct gactor_t* parent, gpoint_t* pos, gpoint_t* size, lcd_image_t img)
{
    actorInit(actor, parent, pos, size);
    actor->paint_call = actorPaintImage;
    actor->img = img;
}

void actorInitButton(struct gactor_t* actor, struct gactor_t* parent, gpoint_t* pos, gpoint_t* size, const char* text, gaction_f* action, int actor_id)
{
    actorInit(actor, parent, pos, size);
    actor->paint_call = actorPaintButton;
    actor->text = text;
    actor->action_call = action;
    actor->is_static = false;
    actor->actor_id = actor_id;
}

void actorInitImgButton(struct gactor_t* actor, struct gactor_t* parent, gpoint_t* pos, gpoint_t* size, lcd_image_t img, const char* text, gaction_f* action, int actor_id)
{
    actorInit(actor, parent, pos, size);
    actor->paint_call = actorPaintButton;
    actor->img = img;
    actor->text = text;
    actor->action_call = action;
    actor->is_static = 0;
    actor->actor_id = actor_id;
}

void actorInitLabel(struct gactor_t* actor, struct gactor_t* parent, gpoint_t* pos, gpoint_t* size, const char* text)
{
    actorInit(actor, parent, pos, size);
    actor->paint_call = actorPaintLabel;
    actor->text = text;
    actor->frame = lfNone;
}

void actorInitActLabel(struct gactor_t* actor, struct gactor_t* parent,
        gpoint_t* pos, gpoint_t* size,
        const char* text,
        gaction_f* action, int actor_id)
{
    actorInit(actor, parent, pos, size);
    actor->paint_call = actorPaintLabel;
    actor->action_call = action;
    actor->text = text;
    actor->frame = lfFlat;
    actor->actor_id = actor_id;
    actor->is_static = 0;
}

void actorPaintLabel(struct gactor_t* actor, gpush_state_t ts)
{
    (void)ts;
    primFrame(actor->gstyle,
            actor->position.x, actor->position.y,
            actor->size.x, actor->size.y,
            actor->frame,
            actor->is_checked || actor->is_focused);
    if( actor->text != NULL )
    {
        const font_struct_t* fs = fontList[actor->gstyle->font_idx];
        int x = 0;
        int y = 0;
        int text_width = strlen(actor->text) *
            (fs->width + fs->space);
        int text_height = fs->height;

        switch(actor->align)
        {
            default:
                x = actor->position.x + 4;
                break;
            case alignCenter:
                x = actor->position.x + (actor->size.x - text_width)/2;
                break;
            case alignRight:
                x = actor->position.x + actor->size.x - text_width - 4;
                break;
        }
        switch(actor->valign)
        {
            default:
                break;
                y = actor->position.y + 4;
            case valignCenter:
                y = actor->position.y + (actor->size.y - text_height)/2;
                break;
            case valignBottom:
                y = actor->position.y + actor->size.y - text_height - 4;
                break;
        }
        primText(actor->gstyle, x, y,
            actor->text, (actor->is_checked || actor->is_focused)?PRIM_FLAG_SELECTED:0 | (actor->is_disable)?PRIM_FLAG_DISABLED:0);
    }
}

void actorPaintFrame(struct gactor_t* actor, gpush_state_t ts)
{
    (void)ts;
    primFrame(actor->gstyle,
            actor->position.x, actor->position.y,
            actor->size.x, actor->size.y,
            actor->frame, actor->is_checked);
}

void actorPaintImage(struct gactor_t* actor, gpush_state_t ts)
{
    (void)ts;
    if(actor->img.storage != imgNone)
    {
        int x, y;
        lcd_coords_t width, height;

        switch(actor->img.storage)
        {
            case imgInternal:
                primImageSize(actor->img.image, &width, &height);
                break;
#if (LCD_ENABLE_SPIFLASH != 0)
            case imgSpiFlash:
                primSfImageSize(actor->img.image, &width, &height);
                break;
#endif
            default:
                break;
        }

        x = actor->position.x + (actor->size.x - width)/2;
        y = actor->position.y + (actor->size.y - height)/2;

        switch(actor->img.storage)
        {
            case imgInternal:
                primImage(actor->img.image, x, y);
                break;
#if (LCD_ENABLE_SPIFLASH != 0)
            case imgSpiFlash:
                primSfImage(actor->img.image, x, y);
                break;
#endif
            default:
                break;
        }
    }
}

void actorPaintButton(struct gactor_t* actor, gpush_state_t ts)
{
    lcd_frame_t ft;
    lcd_coords_t width, height;
    int x, y;
    int shift;

    if( actor->is_checkable )
    {
        if( actor->is_checked )
            { ft = lfLower; shift = 2; }
        else
            { ft = lfRise; shift = 0; }
    }
    else
    {
        if((ts == gpsPush) || (ts == gpsDown))
            { ft = lfLower; shift = 2; }
        else
            { ft = lfRise; shift = 0; }
    }

    primFrame(actor->gstyle,
            actor->position.x, actor->position.y,
            actor->size.x, actor->size.y,
            ft, 0);

    if( actor->img.storage != imgNone )
    {
        switch(actor->img.storage)
        {
            case imgInternal:
                primImageSize(actor->img.image, &width, &height);
                break;
#if (LCD_ENABLE_SPIFLASH != 0)
            case imgSpiFlash:
                primSfImageSize(actor->img.image, &width, &height);
                break;
#endif
            default:
                break;
        }

        x = actor->position.x + 8 /* (actor->size.x - width)/2 */ + shift;
        y = actor->position.y + (actor->size.y - height)/2 + shift;

        switch(actor->img.storage)
        {
            case imgInternal:
                primImage(actor->img.image, x, y);
                break;
#if (LCD_ENABLE_SPIFLASH != 0)
            case imgSpiFlash:
                primSfImage(actor->img.image, x, y);
                break;
#endif
            default:
                break;
        }
        width += 8;
    }
    else
    {
        width = 0;
        height = 0;
    }

    if( actor->text != NULL )
    {
        int x = 0;
        int y = 0;
        int text_width = strlen(actor->text)*8;
        int text_height = 14;

        switch(actor->align)
        {
            default:
                x = actor->position.x + width + 4;
                break;
            case alignCenter:
                x = actor->position.x + /* width + */ (actor->size.x - text_width)/2;
                break;
            case alignRight:
                x = actor->position.x + actor->size.x - text_width - 4;
                break;
        }
        switch(actor->valign)
        {
            default:
                break;
                y = actor->position.y + 4;
            case valignCenter:
                y = actor->position.y + (actor->size.y - text_height)/2;
                break;
            case valignBottom:
                y = actor->position.y + actor->size.y - text_height - 4;
                break;
        }
        x += shift;
        y += shift;
        primText(actor->gstyle, x, y,
                actor->text, 0);
    }
}


void actorCenter(struct gactor_t* actor)
{
    if(actor->parent == NULL)
    {
        actor->position.x = (lcdWidth() - actor->size.x)/2;
        actor->position.y = (lcdHeight() - actor->size.y)/2;
    }
    else
    {
        actor->position.x = actor->parent->position.x + (actor->parent->size.x - actor->size.x)/2;
        actor->position.y = actor->parent->position.y + (actor->parent->size.y - actor->size.y)/2;
    }
}

void actorAlign(struct gactor_t* actor, align_t align, valign_t valign)
{
    lcd_coords_t px, py;
    lcd_coords_t pwidth, pheight;
    lcd_coords_t paddingx, paddingy;
    if(actor->parent == NULL)
    {
        px = 0; py = 0;
        pwidth = lcdWidth();
        pheight = lcdHeight();
        paddingx = 0; paddingy = 0;
    }
    else
    {
        px = actor->parent->position.x;
        py = actor->parent->position.y;
        pwidth = actor->parent->size.x;
        pheight = actor->parent->size.y;
        paddingx = actor->parent->padding.x;
        paddingy = actor->parent->padding.y;
    }

    switch(align)
    {
        case alignLeft:
            actor->position.x = px + paddingx;
            break;
        case alignCenter:
            actor->position.x = px + (pwidth - actor->size.x)/2;
            break;
        case alignRight:
            actor->position.x = px + pwidth - actor->size.x - paddingx;
            break;
        default:
            break;
    }
    switch(valign)
    {
        case valignTop:
            actor->position.y = py + paddingy;
            break;
        case valignCenter:
            actor->position.y = py + (pheight - actor->size.y)/2;
            break;
        case valignBottom:
            actor->position.x = py + pheight - actor->size.y - paddingy;
            break;
        default:
            break;
    }
}

int actorSetFocus(struct gactor_t** actor_list, struct gactor_t* actor)
{
    int actor_id = 0;
    struct gactor_t** p = actor_list;
    while(*p != NULL)
    {
        if(*p == actor)
        {
            actor->is_focused = true;
            actorPaint(actor, gpsNone);
            actor_id = actor->actor_id;
        }
        else if((*p)->is_focused)
        {
            actor->is_focused = false;
            actorPaint(actor, gpsNone);
        }
        p++;
    }
    return actor_id;
}

struct gactor_t* actorSetFocusById(struct gactor_t** actor_list, int id)
{
    struct gactor_t* actor = NULL;
    struct gactor_t** p = actor_list;
    while(*p != NULL)
    {
        if((*p)->actor_id == id)
        {
            actor->is_focused = true;
            actorPaint(actor, gpsNone);
            actor = *p;
        }
        else if((*p)->is_focused)
        {
            actor->is_focused = false;
            actorPaint(actor, gpsNone);
        }
        p++;
    }
    return actor;
}

struct gactor_t* actorGetFocus(struct gactor_t** actor_list)
{
    struct gactor_t** p = actor_list;
    while(*p != NULL)
    {
        if((*p)->is_focused)
        {
            return *p;
        }
        p++;
    }
    return NULL;
}

int actorGetFocusId(struct gactor_t** actor_list)
{
    struct gactor_t** p = actor_list;
    while(*p != NULL)
    {
        if((*p)->is_focused)
        {
            return (*p)->actor_id;
        }
        p++;
    }
    return 0;
}

struct gactor_t* actorGetById(struct gactor_t** actor_list, int id)
{
    struct gactor_t** p = actor_list;
    while(*p != NULL)
    {
        if((*p)->actor_id == id)
        {
            return *p;
        }
        p++;
    }
    return NULL;
}

struct gactor_t* actorFocusNext(struct gactor_t** actor_list)
{
    struct gactor_t** p = actor_list;
    struct gactor_t* cur_actor = NULL;
    struct gactor_t* first_actor = NULL;
    while(*p != NULL)
    {
        if( (!(*p)->is_static) && (!(*p)->is_disable) )
        {
            if( ! first_actor )
            {
                first_actor = *p;
            }
            if(cur_actor)
            {
                (*p)->is_focused = true;
                actorPaint(*p, gpsNone);
                return *p;
            }
            if((*p)->is_focused)
            {
                cur_actor = *p;
                cur_actor->is_focused = false;
                actorPaint(cur_actor, gpsNone);
            }
        }
        p++;
    }
    if(first_actor)
    {
        first_actor->is_focused = true;
        actorPaint(first_actor, gpsNone);
        return first_actor;
    }
    return NULL;
}

struct gactor_t* actorFocusPrev(struct gactor_t** actor_list)
{
    struct gactor_t** p = actor_list;
    struct gactor_t* cur_actor = NULL;
    struct gactor_t* last_actor = NULL;
    while(*p != NULL)
    {
        if( (!(*p)->is_static) && (!(*p)->is_disable) )
        {
            if((*p)->is_focused)
            {
                cur_actor = *p;
                if(last_actor)
                {
                    cur_actor->is_focused = false;
                    last_actor->is_focused = true;
                    actorPaint(cur_actor, gpsNone);
                    actorPaint(last_actor, gpsNone);
                    return last_actor;
                }
            }
            last_actor = *p;
        }
        p++;
    }
    if(last_actor)
    {
        cur_actor->is_focused = false;
        last_actor->is_focused = true;
        actorPaint(cur_actor, gpsNone);
        actorPaint(last_actor, gpsNone);
    }
    return last_actor;
}

