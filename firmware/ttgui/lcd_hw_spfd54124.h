#ifndef _LCD_HW_SPFD54124_H_
#define _LCD_HW_SPFD54124_H_

#include <inttypes.h>

#define LCD_CONTROLLER_STRING "SPFD54124"
#define LCD_GLASS_STRING "Nokia 1616 display"
#define LCD_INTERFACE_STRING "Reduce SPI"

typedef uint8_t lcd_data_t;
typedef int16_t lcd_coords_t;
typedef uint16_t lcd_color_t;

#define lcdWidth()      (160)
#define lcdHeight()     (128)

#endif /* _LCD_HW_SPFD54124_H_ */

