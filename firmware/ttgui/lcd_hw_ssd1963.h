#ifndef _LCD_HW_SSD1963_H_
#define _LCD_HW_SSD1963_H_

typedef uint16_t lcd_data_t;
typedef int16_t lcd_coords_t;
typedef uint16_t lcd_color_t;

#define lcdWidth()      (800)
#define lcdHeight()     (480)

#endif /* _LCD_HW_SSD1963_H_ */

