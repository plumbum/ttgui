#include "lcd_hw.h"
#include "lcd_mcu.h"

#define lcdHwOffsetX()    (0)
#define lcdHwOffsetY()    (0)

#define REG03 0x1000
#define AM (1<<3)
#define ID0 (1<<4)
#define ID1 (1<<5)

#define swapcoord(a, b) do { lcd_coords_t _t = a; a = b; b = _t; } while(0)
// #define swapcoord(x, y) do { x = x ^ y; y = x ^ y; x = x ^ y; } while(0)

void lcdHwInit(void)
{
    lcdMcuInit();
    lcdMcuDelay(200);

#if (LCD_TYPE_ILI9325 == 1)
	lcdMcuWriteReg(0x00E3, 0x3008); // Set internal timing
	lcdMcuWriteReg(0x00E7, 0x0012); // Set internal timing
	lcdMcuWriteReg(0x00EF, 0x1231); // Set internal timing
	lcdMcuWriteReg(0x0000, 0x0001); // Start Oscillation
	lcdMcuWriteReg(0x0001, 0x0100); // set SS and SM bit
	lcdMcuWriteReg(0x0002, 0x0700); // set 1 line inversion

	lcdMcuWriteReg(0x0003, 0x1018); // set GRAM write direction and BGR=0,262K colors,1 transfers/pixel.
	lcdMcuWriteReg(0x0004, 0x0000); // Resize register
	lcdMcuWriteReg(0x0008, 0x0202); // set the back porch and front porch
	lcdMcuWriteReg(0x0009, 0x0000); // set non-display area refresh cycle ISC[3:0]
	lcdMcuWriteReg(0x000A, 0x0000); // FMARK function
	lcdMcuWriteReg(0x000C, 0x0000); // RGB interface setting
	lcdMcuWriteReg(0x000D, 0x0000); // Frame marker Position
	lcdMcuWriteReg(0x000F, 0x0000); // RGB interface polarity
//Power On sequence 
	lcdMcuWriteReg(0x0010, 0x0000); // SAP, BT[3:0], AP, DSTB, SLP, STB
	lcdMcuWriteReg(0x0011, 0x0007); // DC1[2:0], DC0[2:0], VC[2:0]
	lcdMcuWriteReg(0x0012, 0x0000); // VREG1OUT voltage
	lcdMcuWriteReg(0x0013, 0x0000); // VDV[4:0] for VCOM amplitude
	lcdMcuDelay(200); // Dis-charge capacitor power voltage
	lcdMcuWriteReg(0x0010, 0x1690); // SAP, BT[3:0], AP, DSTB, SLP, STB
	lcdMcuWriteReg(0x0011, 0x0227); // R11h=0x0221 at VCI=3.3V, DC1[2:0], DC0[2:0], VC[2:0]
	lcdMcuDelay(50); // delay 50ms
	lcdMcuWriteReg(0x0012, 0x001C); // External reference voltage= Vci;
	lcdMcuDelay(50); // delay 50ms
	lcdMcuWriteReg(0x0013, 0x1800); // R13=1200 when R12=009D;VDV[4:0] for VCOM amplitude
	lcdMcuWriteReg(0x0029, 0x001C); // R29=000C when R12=009D;VCM[5:0] for VCOMH
	lcdMcuWriteReg(0x002B, 0x000D); // Frame Rate = 91Hz
	lcdMcuDelay(50); // delay 50ms
	lcdMcuWriteReg(0x0020, 0x0000); // GRAM horizontal Address
	lcdMcuWriteReg(0x0021, 0x0000); // GRAM Vertical Address
// ----------- Adjust the Gamma Curve ----------//
#if (LCD_GAMMA == 1)
	lcdMcuWriteReg(0x0030, 0x0007);
	lcdMcuWriteReg(0x0031, 0x0302);
	lcdMcuWriteReg(0x0032, 0x0105);
	lcdMcuWriteReg(0x0035, 0x0206);
	lcdMcuWriteReg(0x0036, 0x0808);
	lcdMcuWriteReg(0x0037, 0x0206);
	lcdMcuWriteReg(0x0038, 0x0504);
	lcdMcuWriteReg(0x0039, 0x0007);
	lcdMcuWriteReg(0x003C, 0x0105);
	lcdMcuWriteReg(0x003D, 0x0808);
#elif (LCD_GAMMA == 2)
    lcdMcuWriteReg(0x0030, 0x0000);     // Gamma Control 1
    lcdMcuWriteReg(0x0031, 0x0107);     // Gamma Control 2
    lcdMcuWriteReg(0x0032, 0x0000);     // Gamma Control 3
    lcdMcuWriteReg(0x0035, 0x0203);     // Gamma Control 6
    lcdMcuWriteReg(0x0036, 0x0402);     // Gamma Control 7
    lcdMcuWriteReg(0x0037, 0x0000);     // Gamma Control 8
    lcdMcuWriteReg(0x0038, 0x0207);     // Gamma Control 9
    lcdMcuWriteReg(0x0039, 0x0000);     // Gamma Control 10
    lcdMcuWriteReg(0x003C, 0x0203);     // Gamma Control 13
    lcdMcuWriteReg(0x003D, 0x0403);     // Gamma Control 14
#else
    lcdMcuWriteReg(0x0030, 0x0000);
    lcdMcuWriteReg(0x0031, 0x0506);
    lcdMcuWriteReg(0x0032, 0x0104);
    lcdMcuWriteReg(0x0035, 0x0207);
    lcdMcuWriteReg(0x0036, 0x000F);
    lcdMcuWriteReg(0x0037, 0x0306);
    lcdMcuWriteReg(0x0038, 0x0102);
    lcdMcuWriteReg(0x0039, 0x0707);
    lcdMcuWriteReg(0x003C, 0x0702);
    lcdMcuWriteReg(0x003D, 0x1604);
#endif
//------------------ Set GRAM area ---------------//
	lcdMcuWriteReg(0x0050, 0x0000); // Horizontal GRAM Start Address
	lcdMcuWriteReg(0x0051, 0x00EF); // Horizontal GRAM End Address
	lcdMcuWriteReg(0x0052, 0x0000); // Vertical GRAM Start Address
	lcdMcuWriteReg(0x0053, 0x013F); // Vertical GRAM Start Address
	lcdMcuWriteReg(0x0060, 0xA700); // Gate Scan Line
	lcdMcuWriteReg(0x0061, 0x0001); // NDL,VLE, REV
	lcdMcuWriteReg(0x006A, 0x0000); // set scrolling line
//-------------- Partial Display Control ---------//
	lcdMcuWriteReg(0x0080, 0x0000);
	lcdMcuWriteReg(0x0081, 0x0000);
	lcdMcuWriteReg(0x0082, 0x0000);
	lcdMcuWriteReg(0x0083, 0x0000);
	lcdMcuWriteReg(0x0084, 0x0000);
	lcdMcuWriteReg(0x0085, 0x0000);
//-------------- Panel Control -------------------//
	lcdMcuWriteReg(0x0090, 0x0010);
	lcdMcuWriteReg(0x0092, 0x0000);
	lcdMcuWriteReg(0x0093, 0x0003);
	lcdMcuWriteReg(0x0095, 0x0110);
	lcdMcuWriteReg(0x0097, 0x0000);
	lcdMcuWriteReg(0x0098, 0x0000);
	lcdMcuWriteReg(0x0007, 0x0133); // 262K color and display ON

    lcdMcuWriteReg(0x0003, REG03 | AM | ID0); // set GRAM write direction and BGR=0,262K colors,1 transfers/pixel.
#else
#error "Unsupported chip"
#endif
}

/*
void dispRotate(display_rotate_t ang)
{
    switch(ang)
    {
        default:
            _lcdWriteReg(0x0003, REG03 | AM | ID1); // set GRAM write direction and BGR=0,262K colors,1 transfers/pixel.
            break;
        case ang90:
            _lcdWriteReg(0x0003, REG03 | 0); // set GRAM write direction and BGR=0,262K colors,1 transfers/pixel.
            break;
        case ang180:
            _lcdWriteReg(0x0003, REG03 | AM | ID0); // set GRAM write direction and BGR=0,262K colors,1 transfers/pixel.
            break;
        case ang270:
            _lcdWriteReg(0x0003, REG03 | ID1 | ID0); // set GRAM write direction and BGR=0,262K colors,1 transfers/pixel.
            break;
    }
}
*/

void lcdHwIdleMode(char on)
{
#if 0    
    lcdMcuBusGrab();
    if(on)
        lcdMcuCmd (0x39); /* Idle mode on */
    else
        lcdMcuCmd (0x38); /* Idle mode off */
    lcdMcuBusRelease();
#endif
}

void lcdHwMDA(uint8_t d)
{
#if 0
    lcdMcuBusGrab();
    lcdMcuCmd (0x36); /* Memory data access control */
    lcdMcuData(d);
    lcdMcuBusRelease();
#endif
}


void lcdHwStartFrame(
        lcd_coords_t left, lcd_coords_t top,
        lcd_coords_t right, lcd_coords_t bottom)
{
    lcd_coords_t l, r, t, b;

    l = lcdHwWidth()-1 - left;
    r = lcdHwWidth()-1 - right;
    t = top;
    b = bottom;

    lcdMcuBusGrab();
    /* Column frames */
    lcdMcuWriteReg(0x52, r);
    lcdMcuWriteReg(0x53, l);

    /* Row frames */
    lcdMcuWriteReg(0x50, t);
    lcdMcuWriteReg(0x51, b);

    /* Start coordinate */
    lcdMcuWriteReg(0x20, t);
    lcdMcuWriteReg(0x21, l);

    /* Prepare for write */
    lcdMcuCmd(0x22);
}

void lcdHwEndFrame(void)
{
    lcdMcuBusRelease();
}

void lcdHwPutPixel(lcd_color_t color)
{
    lcdMcuData(color);
}

void lcdHwFillRect(
        lcd_coords_t x1, lcd_coords_t y1,
        lcd_coords_t x2, lcd_coords_t y2,
        lcd_color_t color)
{
    if(x1 > x2) swapcoord(x1, x2);
    if(y1 > y2) swapcoord(y1, y2);

    lcdHwStartFrame(x1, y1, x2, y2);
    int i, j;
    for(j=y1; j<=y2; j++)
    {
        for(i=x1; i<=x2; i++)
        {
            lcdHwPutPixel(color);
        }
    }
    lcdHwEndFrame();
}

void lcdHwFillScreen(lcd_color_t color)
{
    lcdHwStartFrame(0, 0, lcdHwWidth()-1, lcdHwHeight()-1);
    int i;
    for(i=0; i<lcdHwWidth()*lcdHwHeight(); i++)
    {
        lcdHwPutPixel(color);
    }
    lcdHwEndFrame();
}

void lcdHwPutBitmap(
        lcd_coords_t x, lcd_coords_t y,
        lcd_coords_t w, lcd_coords_t h,
        const uint8_t* data,
        lcd_color_t fg, lcd_color_t bg)
{
    int i, j;
    uint8_t bc = 7;
    uint8_t bits = *data++;
    lcdHwStartFrame(x, y, x+w-1, y+h-1);
    for(j=0; j<h; j++)
    {
        for(i=0; i<w; i++)
        {
            if(bits & 0x80)
                lcdHwPutPixel(fg);
            else
                lcdHwPutPixel(bg);
            if(bc)
            {
                bits <<= 1;
                bc--;
            }
            else
            {
                bits = *data++;
                bc = 7;
            }
        }
    }
    lcdHwEndFrame();
}

#if 0
void lcdHwChar(lcd_coords_t x, lcd_coords_t y, char c, lcd_color_t fg, lcd_color_t bg)
{
    int i, j;
    unsigned char* pC = (unsigned char*)font8x16 + c*16;
    unsigned char bits;
    lcdHwStartFrame(x, y, x+7, y+15);
    for(j=0; j<16; j++)
    {
        bits = *pC++;
        for(i=0; i<8; i++)
        {
            if(bits & 0x80)
                lcdHwPutPixel(fg);
            else
                lcdHwPutPixel(bg);
            bits <<= 1;
        }
    }
    lcdHwEndFrame();
}

void lcdHwChar5(lcd_coords_t x, lcd_coords_t y, char c, lcd_color_t fg, lcd_color_t bg)
{
    char i, j;
    unsigned char* pC = (unsigned char*)font5x8 + c*5;
    unsigned char bits;
    lcdHwMDA(MX);
    lcdHwStartFrame(y, x, y+7, x+5);
    // lcdHwStartFrame(x, y, x+4, y+7);
    for(j=0; j<5; j++)
    {
        bits = *pC++;
        for(i=0; i<8; i++)
        {
            if(bits & 0x01)
                lcdHwPutPixel(fg);
            else
                lcdHwPutPixel(bg);
            bits >>= 1;
        }
    }
    for(i=0; i<8; i++)
    {
        lcdHwPutPixel(bg);
    }
    lcdHwEndFrame();
    lcdHwMDA(MX | MV);
}

void lcdHwPutBitmapVertByte(
        lcd_coords_t x, lcd_coords_t y,
        lcd_coords_t w, lcd_coords_t h8,
        uint8_t* data,
        lcd_color_t fg, lcd_color_t bg)
{
    int i, j;
    uint8_t bc;
    uint8_t bits;
    lcdHwMDA(MX);
    lcdHwStartFrame(y, x, y+(h8*8-1), x+w-1);
    for(j=0; j<w; j++)
    {
        for(i=0; i<h8; i++)
        {
            bits = *data++;
            for(bc=0; bc<8; bc++)
            {
                if(bits & 0x80)
                    lcdHwPutPixel(fg);
                else
                    lcdHwPutPixel(bg);
                bits <<= 1;
            }
        }
    }
    lcdHwEndFrame();
    lcdHwMDA(MX | MV);
}
#endif

