#ifndef _LCD_HW_H_
#define _LCD_HW_H_

#include <inttypes.h>

#include "lcd_conf.h"

#if (LCD_TYPE == LCD_TYPE_SPFD54124)
#   include "lcd_hw_spfd54124.h"
#elif (LCD_TYPE == LCD_TYPE_ILI9325)
#   include "lcd_hw_ili9325.h"
#elif (LCD_TYPE == LCD_TYPE_SSD1963)
#   include "lcd_hw_ssd1963.h"
#else
#error "Unknow LCD_TYPE_x"
#endif

/*
typedef enum {
    lrNormal = 0,
    lrCW90,
    lrCW180,
    lrCW270,
} lcd_rotation_t;

typedef enum {
    dirToRight = 0,
    dirToBottom
} lcd_axis_direction_t;
*/

#define lcdBlack        lcdHwRGB(0,    0,    0)
#define lcdDarkGray     lcdHwRGB(0x66, 0x66, 0x66)
#define lcdGray         lcdHwRGB(0x99, 0x99, 0x99)
#define lcdLightGray    lcdHwRGB(0xCC, 0xCC, 0xCC)
#define lcdWhite        lcdHwRGB(255,  255,  255)

#define lcdNightBlue    lcdHwRGB(0,    0,    0x66)
#define lcdBlue         lcdHwRGB(0,    0,    255)
#define lcdGreen        lcdHwRGB(0,    255,  0)
#define lcdCyan         lcdHwRGB(0,    255,  255)
#define lcdRed          lcdHwRGB(255,  0,    0)
#define lcdMagenta      lcdHwRGB(255,  0,    255)
#define lcdYellow       lcdHwRGB(255,  255,  0)

void lcdHwInit(void);

#define lcdHwRGB(r, g, b) (lcd_color_t)((((r)&0xF8)<<8) | (((g)&0xFC)<<3) | ((b)>>3))

void lcdHwIdleMode(char on);

/* Base control */
void lcdHwStartFrame(
        lcd_coords_t left, lcd_coords_t top,
        lcd_coords_t right, lcd_coords_t bottom);
void lcdHwEndFrame(void);
void lcdHwPutPixel(lcd_color_t color);


/* Macro functions */
void lcdHwFillScreen(lcd_color_t color);
void lcdHwFillRect(
        lcd_coords_t x1, lcd_coords_t y1,
        lcd_coords_t x2, lcd_coords_t y2,
        lcd_color_t color);

void lcdHwPutBitmap(
        lcd_coords_t x, lcd_coords_t y,
        lcd_coords_t w, lcd_coords_t h,
        const uint8_t* data,
        lcd_color_t fg, lcd_color_t bg);

#endif /* _LCD_HW_H_ */

