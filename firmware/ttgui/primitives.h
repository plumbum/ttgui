#ifndef _PRIMITIVES_H_
#define _PRIMITIVES_H_

#include "lcd_hw.h"
#include "font.h"

typedef enum {
    lfNone,
    lfFlat,
    lfRise,
    lfLower,
    lfDisabled
} lcd_frame_t;

typedef struct {
    lcd_color_t text;
    lcd_color_t frame_bg;
    lcd_color_t bg;
    lcd_color_t rise1;
    lcd_color_t rise2;
    lcd_color_t fade1;
    lcd_color_t fade2;
    lcd_color_t gray_text;
    lcd_color_t sel_text;
    lcd_color_t sel_bg;
    int font_idx;
} lcd_style_t;

typedef enum {
    imgNone = 0,
    imgInternal,
    imgSpiFlash,
    imgSD
} lcd_image_storage_t;

typedef struct {
    lcd_image_storage_t storage;
    const uint16_t* image;
} lcd_image_t;

#define PRIM_FLAG_SELECTED       1
#define PRIM_FLAG_DISABLED       2
#define PRIM_FLAG_NOBG           4
#define PRIM_FLAG_TRANSPARENT    8

extern const lcd_style_t defaultStyle;

void primFillRect(lcd_coords_t x, lcd_coords_t y,
        lcd_coords_t w, lcd_coords_t h,
        lcd_color_t color);

void primLineHoriz(lcd_coords_t x, lcd_coords_t y,
        lcd_coords_t w,
        lcd_color_t color);

void primLineVert(lcd_coords_t x, lcd_coords_t y,
        lcd_coords_t h,
        lcd_color_t color);

void primFrame(const lcd_style_t* style, lcd_coords_t x, lcd_coords_t y,
        lcd_coords_t w, lcd_coords_t h,
        lcd_frame_t type, int flags);

void primText(const lcd_style_t* style,
        lcd_coords_t x, lcd_coords_t y,
        const char* text, int flags);

void primImage(const uint16_t* addr, lcd_coords_t x, lcd_coords_t y);
void primImageSize(const uint16_t* addr, lcd_coords_t* width, lcd_coords_t* height);

#if (LCD_ENABLE_SPIFLASH != 0)
void primSfImage(const uint16_t* sf_addr, lcd_coords_t x, lcd_coords_t y);
void primSfImageSize(const uint16_t* sf_addr, lcd_coords_t* width, lcd_coords_t* height);
#endif

#endif /* _PRIMITIVES_H_ */
