#include "font.h"


/* import native fonts */
#include "terminus_14b.h"
#include "small_5x8.h"

/* Available fonts */
const font_struct_t* fontList[] = 
{
    &font_terminus_14b,
    &font_small_5x8,
    0
};

/* Routines */

void fontChar(const font_struct_t* font, lcd_coords_t x, lcd_coords_t y, char c, lcd_color_t fg, lcd_color_t bg)
{
    const unsigned char* pGlyph;
    if(font->index != 0)
    {
        /* Indexed glyphs table */
        pGlyph = font->glyphs + font->index[(unsigned char)c];
    }
    else
    {
        /* Linear glyphs table */
        pGlyph = font->glyphs + (unsigned char)c * ((font->height * font->width)>>3);
    }
    lcdHwPutBitmap(x, y, font->width, font->height, pGlyph, fg, bg);
}

void fontString(const font_struct_t* font, lcd_coords_t x, lcd_coords_t y, const char* str, lcd_color_t fg, lcd_color_t bg)
{
    char c;
    const unsigned char* pGlyph;
    if(font->index != 0)
    {
        while((c = *str++) != 0)
        {
            /* Indexed glyphs table */
            pGlyph = font->glyphs + font->index[(unsigned char)c];
            lcdHwPutBitmap(x, y, font->width, font->height, pGlyph, fg, bg);
            x += font->width + font->space;
        }
    }
    else
    {
        int coef = (font->height * font->width)>>3;
        while((c = *str++) != 0)
        {
            /* Linear glyphs table */
            pGlyph = font->glyphs + (unsigned char)c * coef;
            lcdHwPutBitmap(x, y, font->width, font->height, pGlyph, fg, bg);
            x += font->width + font->space;
        }
    }
}

#define COLS 100
#define ROWS 34
int crow = 0;
int ccol = 0;
lcd_color_t foreground = lcdBlack;
lcd_color_t background = lcdHwRGB(255, 255, 192);

void lputchar(char c)
{
    switch(c)
    {
        case 0x0C: // New page
            ccol = 0;
            crow = 0;
            lcdHwFillScreen(background);
            break;
        case 0x0D: // Carridge return 
            ccol = 0;
            break;
        case 0x0A: // Line feed
            if(crow < ROWS)
                crow++;
            break;
        case 0x08: // Backspace
            if(ccol > 0)
                ccol--;
            break;
        case 0x09: // Tabulation
            ccol += 8;
            ccol &= ~7;
            if(ccol >= COLS)
            {
                ccol = 0;
                if(crow < ROWS)
                    crow++; 
            }
            break;
        case 0x0E: // Shift out (red color)
            foreground = lcdRed;
            break;
        case 0x0F: // Shift in (black color)
            foreground = lcdBlack;
            break;
        case 0x17: // End of text block (clean current line)
            lcdHwFillRect(0, crow*14, lcdWidth()-1, crow*14+13, background);
            break;
        default:
            fontChar(fontList[0], ccol*8, crow*14, c, foreground, background);
            ccol++;
            if(ccol >= COLS)
            {
                ccol = 0;
                if(crow < ROWS)
                    crow++; 
            }
    }
}

void lputs(const char* str)
{
    char c;
    while((c = *str++) != 0)
    {
        lputchar(c);
    }
}
