#ifndef _LCD_MCU_H_H_
#define _LCD_MCU_H_H_

#define __inline static inline

/* Must be implemented in user program */
#if 0
__inline void lcdMcuInit(void);

__inline void lcdMcuBusGrab(void);
__inline void lcdMcuBusRelease(void);

__inline void lcdMcuCmd(lcd_data_t cmd);
__inline void lcdMcuData(lcd_data_t data);
__inline void lcdMcuWriteReg(lcd_data_t reg, lcd_data_t data);

__inline lcd_data_t lcdMcuReadStatus(void);
__inline lcd_data_t lcdMcuReadData(void);
__inline lcd_data_t lcdMcuReadReg(lcd_data_t reg);

__inline int lcdMcuFillBlock(lcd_data_t data, uint32_t len);
__inline int lcdMcuCopyBlock(lcd_data_t* data, uint32_t len);
#endif

#endif /* _LCD_MCU_H_H_ */

