#ifndef _SPIFLASH_H_
#define _SPIFLASH_H_

#include <inttypes.h>

void sfInit(void);

void sfReadBlock(uint8_t* buf, int addr, int len);
void sfWriteBlock(uint8_t* buf, int addr, int len);

void sfWriteStatus(uint8_t stat);

void sfWaitFlash(void);
uint32_t sfReadJedecId(void);

void sfEraseChip(void);
void sfErase4k(uint32_t addr);

void sfSetDataWidth(uint8_t width);

/* Internal funcitons */
void sfWriteEnable(void);
void sfWriteDisable(void);


uint16_t sfSpiExchange(uint16_t data);
void sfSpiCsGrab(void);
void sfSpiCsRelease(void);

#endif

