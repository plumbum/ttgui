#ifndef _LCD_MCU_H_
#define _LCD_MCU_H_

#include "lcd_mcu_h.h"
#include "stm32f10x.h"

#define Bank1_LCD_C    ((uint32_t)0x60000000)	 //display Reg ADDR
#define Bank1_LCD_D    ((uint32_t)0x60020000)    //display Data ADDR

#define BANK 0

__inline void lcdMcuDelay(int parrots)
{
    while(parrots--)
    {
        volatile int i;
        for(i=0; i<5000; i++) {}
    }
}

__inline void lcdMcuInit(void)
{
    GPIOE->BRR = (1<<1);

    RCC->AHBENR |= RCC_AHBENR_FSMCEN | RCC_AHBENR_DMA1EN;
    //FSMC_Bank1->BTCR[0] = 0;
    //FSMC_Bank1E->BWTR[0] = 0;

    // Register clear
    // Bank1 have NE1 ~ 4, each has a BCR + TCR, so a total of eight registers.
    // Here we use the NE2, also corresponds to BTCR [6], [7].
    FSMC_Bank1->BTCR[BANK*2] = 0x00000000;
    FSMC_Bank1->BTCR[BANK*2+1] = 0x00000000;
    FSMC_Bank1E->BWTR[BANK*2] = 0x00000000;
    // Register to use asynchronous mode of operation of BCR
    FSMC_Bank1->BTCR[BANK*2] |= FSMC_BCR1_WREN; // memory write enable
    FSMC_Bank1->BTCR[BANK*2] |= FSMC_BCR1_MWID_0; // memory data width is 16bit
    // Operation BTR register
    FSMC_Bank1->BTCR[BANK*2+1] |= (2<<8) | (0<<0); // data retention time for 3 HCLK
    // Flash Write Timing Register
    FSMC_Bank1E->BWTR[BANK*2] = 0x0FFFFFFF; // default values
    // Enable BANK4 (PC card Device)
    FSMC_Bank1->BTCR[BANK*2] |= FSMC_BCR1_MBKEN;

    lcdMcuDelay(50);
    GPIOE->BSRR = (1<<1);
    lcdMcuDelay(50);

}

#define lcdMcuBusGrab()
#define lcdMcuBusRelease()
/*
__inline void lcdMcuBusGrab(void)
{
}

__inline void lcdMcuBusRelease(void)
{
}
*/

#if defined(DOXYGEN)
__inline void lcdMcuCmd(lcd_data_t cmd)
{
    /* Send address bit: 0 - command */
    *(__IO uint16_t *) (Bank1_LCD_C) = cmd;	
}
#else
#define lcdMcuCmd(cmd) *(__IO uint16_t *) (Bank1_LCD_C) = (cmd)
#endif

#if defined(DOXYGEN)
__inline void lcdMcuData(lcd_data_t data)
{
    /* Send address bit: 1 - command */
    *(__IO uint16_t *) (Bank1_LCD_D) = data;	
}
#else
#define lcdMcuData(data) *(__IO uint16_t *) (Bank1_LCD_D) = (data)
#endif

__inline void lcdMcuWriteReg(lcd_data_t reg, lcd_data_t data)
{
    *(__IO uint16_t *) (Bank1_LCD_C) = reg;	
    *(__IO uint16_t *) (Bank1_LCD_D) = data;	
}

__inline lcd_data_t lcdMcuReadStatus(void)
{
    return *(__IO uint16_t *) (Bank1_LCD_C);
}

__inline lcd_data_t lcdMcuReadData(void)
{
    return *(__IO uint16_t *) (Bank1_LCD_D);
}

__inline lcd_data_t lcdMcuReadReg(lcd_data_t reg)
{
    *(__IO uint16_t *) (Bank1_LCD_C) = reg;	
    return *(__IO uint16_t *) (Bank1_LCD_D);
}


__inline int lcdMcuFillBlock(lcd_data_t data, uint32_t len)
{
    uint16_t l = 0x7FF0;

    DMA1_Channel7->CPAR = (uint32_t)&data;
    DMA1_Channel7->CMAR = Bank1_LCD_D;
    while(len != 0)
    {
        if(len < l) l = len;
        len -= l;

        DMA1->IFCR |= DMA_IFCR_CGIF7;

        DMA1_Channel7->CNDTR = l*2; /* Transfer size */
        DMA1_Channel7->CCR = DMA_CCR7_MEM2MEM |
            DMA_CCR7_PL_1 | /* High priority */
            DMA_CCR7_MSIZE_0 | /* 16 bits */
            DMA_CCR7_PSIZE_0 | /* 16 bits */
            // DMA_CCR7_DIR | /* read from memory */
            DMA_CCR7_EN; // Start transfer

        while((DMA1->ISR & DMA_ISR_GIF7) == 0)
            ;

        DMA1_Channel7->CCR = 0;
    }
    return 0;
}

__inline int lcdMcuCopyBlock(lcd_data_t* data, uint32_t len)
{
    uint16_t l = 0x7FF0;

    DMA1_Channel7->CMAR = Bank1_LCD_D; /* Dest */
    while(len != 0)
    {
        if(len < l) l = len;
        len -= l;

        DMA1_Channel7->CPAR = (uint32_t)data; /* Source */
        DMA1_Channel7->CNDTR = l*2; /* Transfer size */

        DMA1->IFCR |= DMA_IFCR_CGIF1;
        DMA1_Channel7->CCR = DMA_CCR7_MEM2MEM |
            DMA_CCR7_PL_1 | /* High priority */
            DMA_CCR7_MSIZE_0 | /* 16 bits destination */
            DMA_CCR7_PSIZE_0 | /* 16 bits source */
            DMA_CCR7_PINC | /* Increment source */
            // DMA_CCR7_DIR | /* read from memory */
            DMA_CCR7_EN; // Start transfer

        while((DMA1->ISR & DMA_ISR_TCIF7) == 0)
            ;

        DMA1_Channel7->CCR = 0;

        data += l*2;
    }
    return 0;
}

#endif /* _LCD_MCU_H_ */

