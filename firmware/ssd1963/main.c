/* STM32 includes */
#include <stm32f10x.h>
#ifdef USE_STDPERIPH_DRIVER
    #include <stm32f10x_conf.h>
#endif

#include "lcd_hw.h"
#include "lcd_mcu.h"
#include "utils.h"
#include "font.h"
#include "primitives.h"

#include "touch.h"

#include "spiflash.h"

void delay(void);
int main(void);
static void main_noreturn(void) __attribute__((noreturn));


/*
 * IO pins assignments.
 */
#define GPIOB_LED               5
/*
 * Perif access routines
 */
#define LED_ON() GPIOB->BSRR = (1<<GPIOB_LED)
#define LED_OFF() GPIOB->BRR = (1<<GPIOB_LED)


/*
#include "stepaha.h"
// #include "wine.h"
*/

/*
void putImage(lcd_coords_t x, lcd_coords_t y)
{
    lcd_coords_t xc, yc;
    uint8_t pix[3];
    const char* d = header_data;

    lcdHwStartFrame(x, y, x+width-1, y+height-1);
    for(yc=0; yc<width; yc++)
    {
        for(xc=0; xc<height; xc++)
        {
            HEADER_PIXEL(d, pix);
            lcdHwPutPixel(lcdHwRGB(pix[0], pix[1], pix[2]));
        }
    }
    lcdHwEndFrame();

}
*/

/*
const
#include "earth.h"
*/

char szBuf[256];

void putRleImage(lcd_coords_t x, lcd_coords_t y)
{
    int i;
    uint16_t width, height;
    lcd_color_t color;
    lcd_data_t cnt;

    sfSpiCsGrab();
    sfSpiExchange(0x0B);
    sfSpiExchange(0x00);
    sfSpiExchange(0x00);
    sfSpiExchange(0x00);
    sfSpiExchange(0xFF);

    sfSetDataWidth(16);
    width = sfSpiExchange(0);
    height = sfSpiExchange(0);
    width = (width>>8) | (width<<8);
    height = (height>>8) | (height<<8);

    lcdHwStartFrame(x, y, x+width-1, y+height-1);
    for(i=0; i<width*height;)
    {
        cnt = sfSpiExchange(0);
        cnt = (cnt>>8) | (cnt<<8);
        if(cnt & 0x8000)
        {
            cnt &= 0x7FFF;
            if(cnt == 0) cnt = 0x8000;
            color = sfSpiExchange(0);
            color = (color>>8) | (color<<8);
            i += cnt;
            while(cnt--)
            {
                lcdHwPutPixel(color);
            }
        }
        else
        {
            if(cnt == 0) cnt = 0x8000;
            i += cnt;
            while(cnt--)
            {
                color = sfSpiExchange(0);
                color = (color>>8) | (color<<8);
                lcdHwPutPixel(color);
            }
        }

    }
    sfSetDataWidth(8);
    lcdHwEndFrame();
    sfSpiCsRelease();
}


const char hello_world[] = "Hello world!";

/**
 * Main function
 */
int main(void)
{
	main_noreturn();
}

inline void main_noreturn(void)
{	
    RCC->APB2ENR |=
        RCC_APB2ENR_IOPAEN | RCC_APB2ENR_IOPBEN | RCC_APB2ENR_IOPCEN |
        RCC_APB2ENR_IOPDEN | RCC_APB2ENR_IOPEEN | RCC_APB2ENR_IOPFEN |
        RCC_APB2ENR_IOPGEN;

    GPIOA->BSRR = 0xFFFFFFFF;
    GPIOA->CRL = 0x88888888;
    GPIOA->CRH = 0x88888888;

    GPIOB->BSRR = 0xFFFFFFFF;
    GPIOB->CRL = 0x38388888;
    GPIOB->CRH = 0x88888888;

    GPIOC->BSRR = 0xFFFFFFFF;
    GPIOC->CRL = 0x88888888;
    GPIOC->CRH = 0x44888888;

    GPIOD->BSRR = 0xFFFFFFFF;
    GPIOD->CRL = 0xb8bb88bb;
    GPIOD->CRH = 0xbb38bbbb;

    GPIOE->BSRR = 0xFFFFFFFF;
    GPIOE->CRL = 0xb8888838;
    GPIOE->CRH = 0xbbbbbbbb;

    GPIOF->BSRR = 0xFFFFFFFF;
    GPIOF->CRL = 0x88888888;
    GPIOF->CRH = 0x88888888;

    GPIOG->BSRR = 0xFFFFFFFF;
    GPIOG->CRL = 0x88888888;
    GPIOG->CRH = 0x88888888;


    lcdHwInit();

    lcdHwFillScreen(lcdHwRGB(0, 192, 192));
    fontString(fontList[0], 0, 0, "Initialization...", lcdNightBlue, lcdYellow);

    sfInit();

    // sfWriteStatus(0);
    // sfEraseChip();
    // sfWriteBlock(earth_rle, 0, earth_rle_len);
    // sfWaitFlash();
    // putRleImage(0, 0);

    fontString(fontList[0], 48, 32, "������ ���!", lcdNightBlue, lcdYellow);
    fontString(fontList[0], 48, 32+14, hello_world, lcdNightBlue, lcdYellow);
    fontString(fontList[0], lcdHwWidth()-8*(sizeof(hello_world)-1), lcdHwHeight()-16, hello_world, lcdNightBlue, lcdYellow);

    uint32_t id = sfReadJedecId();


    int i;
    for(i=0; i<4; i++)
    {
        uitox((id>>(8*i)), szBuf, 2);
        fontString(fontList[0], 64, 128+14*i, szBuf, lcdNightBlue, lcdYellow);
    }

    uint8_t sr;
    sfSpiCsGrab();
    sfSpiExchange(0x05); // Read status register
    while(1)
    {
        sr = sfSpiExchange(0xFF);
        uitox(sr, szBuf, 2);
        fontString(fontList[0], 64, 64, szBuf, lcdNightBlue, lcdYellow);
    }
    sfSpiCsRelease();

    // touchInit();

    uint32_t cnt = 0;
    int shift;
    int x, y;
    int ax, ay;
    ax = 0;
    ay = 0;
    while(1)
    {
        /*
        touchGetXY(&x, &y);
        ax += x; ay += y;

        if((cnt & 0x00) == 0x00)
        {
            // memset(szBuf, 0, 5);
            uitox((ax/1), szBuf, 4);
            // szBuf[strlen(szBuf)] = ' ';
            // szBuf[5] = 0;
            fontString(fontList[0], 16, 200, szBuf, lcdBlue, lcdYellow);

            // memset(szBuf, 0, 5);
            uitox((ay/1), szBuf, 4);
            // szBuf[strlen(szBuf)] = ' ';
            // szBuf[5] = 0;
            fontString(fontList[0], 16, 214, szBuf, lcdRed, lcdYellow);

            ax = 0; ay = 0;
        }
        */

        // putRleImage(earth_rle, 0, 0);

        uitoa(cnt, szBuf);
        /*
        if((cnt & 0xFF) == 0)
        {
            if(cnt & 0x100)
            {
                primFrame(&defaultStyle, 4, 60, 64, 32, lfRise);
                shift = 0;
            }
            else
            {
                primFrame(&defaultStyle, 4, 60, 64, 32, lfLower);
                shift = 2;
            }
        }
        */
        fontString(fontList[0], 16+shift, 62+shift, szBuf, lcdBlack, lcdLightGray);
        fontString(fontList[1], 24+shift, 78+shift, szBuf, lcdBlack, lcdLightGray);
        cnt++;

    }
}

void delay(void) 
{
	int i = 100000;													/* About 1/4 second delay */
while (i-- > 0) {
    asm("nop");													/* This stops it optimising code out */
}
}

