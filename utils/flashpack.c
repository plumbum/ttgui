#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <string.h>


uint32_t* sizes;
uint32_t* addresses;

char szDefineName[128];

void writeDWord(uint32_t d, FILE* f)
{
    /* LSB */
    fputc((d>>0) & 0xFF, f);
    fputc((d>>8) & 0xFF, f);
    fputc((d>>16) & 0xFF, f);
    fputc((d>>24) & 0xFF, f);
}

void translateFileName(const char* from, char* to)
{
    char c;
    const char* slash = strrchr(from, '/');
    if(slash == NULL)
        slash = from;
    else
        slash++;
    strncpy(to, slash, sizeof(szDefineName));
    while((c = *to) != 0)
    {
        if(isalnum(c))
            *to++ = toupper(c);
        else
            *to++ = '_';
    }
}

int main(int argc, char** argv)
{
    FILE* fout;
    FILE* fin;
    int i;
    int idx;
    uint32_t addr;
    uint32_t isize;

    if(argc<3)
    {
        fprintf(stderr, "Usage: %s <outfile.bin> <inputfile1.rle> [...]\n", argv[0]);
        return 9;
    }

    isize = (argc-2);
    sizes = malloc(isize*4);
    addresses = malloc(isize*4);
    /* First dword is file counts
     * addr (4)
     * size (4)
     */
    addr = 4 + isize*2*4;
    idx = 0;
    for(i=2; i<argc; i++)
    {
        int s;
        fin = fopen(argv[i], "rb");
        if(fin < 0)
        {
            fprintf(stderr, "Can't open input file %s\n", argv[i]);
            fclose(fin);
            free(addresses);
            free(sizes);
            return 2;
        }
        fseek(fin, 0, SEEK_END);
        s = ftell(fin);
        fclose(fin);

        sizes[idx] = s;
        addresses[idx] = addr;
        idx++;
        addr += s;
    }

    fout = fopen(argv[1], "wb");
    if(fout < 0)
    {
        fprintf(stderr, "Can't open output file %s\n", argv[1]);
        return 1;
    }

    /* Write item count */
    writeDWord(isize, fout);

    /* Write file table */
    for(i=0; i<isize; i++)
    {
        writeDWord(addresses[i], fout);
        writeDWord(sizes[i], fout);
    }

    printf("#define FILES_COUNT (%d)\n\n", isize);
    /* Copy files */
    for(i=0; i<isize; i++)
    {
        fin = fopen(argv[i+2], "rb");
        if(fin < 0)
        {
            fprintf(stderr, "Can't copy input file %s\n", argv[i+2]);
            fclose(fout);
            fclose(fin);
            free(addresses);
            free(sizes);
            return 3;
        }
        uint8_t* buf = malloc(sizes[i]);
        size_t len = fread(buf, 1, sizes[i], fin);
        fwrite(buf, 1, len, fout);
        free(buf);
        fclose(fin);
        printf("/* File '%s' has size %d plase at 0x%X */\n", argv[i+2], sizes[i], addresses[i]);
        translateFileName(argv[i+2], szDefineName);
        printf("#define %s %d\n\n", szDefineName, i);
    }

    printf("\n/* Total size %ld (0x%lX) */", ftell(fout), ftell(fout));

    free(addresses);
    free(sizes);
    fclose(fout);
    return 0;
}

