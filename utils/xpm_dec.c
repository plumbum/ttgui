#include "io-xpm.h"

#define packRGB(clr) ( \
    ((clr).red & 0xF8) << 8 | \
    ((clr).green & 0xFC) << 3 | \
    ((clr).blue & 0xF8) >> 3 )

typedef enum {
    boLSB = 0,
    boMSB,
} byte_order_t;

void writeWord(FILE* f, uint16_t w, byte_order_t order)
{
    if(order == boLSB)
    {
        fputc(w & 0xFF, f);
        fputc((w>>8) & 0xFF, f);
    }
    else
    {
        fputc((w>>8) & 0xFF, f);
        fputc(w & 0xFF, f);
    }
}

int main(int argc, char** argv)
{
    FILE* fxpm;
    FILE* frle;

    int debug = 0;
    byte_order_t order = boLSB;

    if(argc<2)
    {
        fprintf(stderr, "Usage: %s <infile.xpm> <outfile.rle>\n", argv[0]);
        return 1;
    }

    printf("argc %d\n", argc);
    // if(argc == 4) debug = 1;

    fxpm = fopen(argv[1], "r");
    if(fxpm <= 0)
    {
        fprintf(stderr, "Can't open input file %s\n", argv[1]);
        return 1;
    }

    frle = fopen(argv[2], "wb");
    if(frle <= 0)
    {
        fprintf(stderr, "Can't open output file %s\n", argv[2]);
        return 1;
    }

    if(strcmp(argv[1], argv[2]) == 0)
    {
        fprintf(stderr, "Can't write file itself: %s\n", argv[1]);
        return 1;
    }

    char* buf;
    int w;
    int h;
    int t;

    buf = loader_xpm(fxpm, &w, &h, &t);
    if(buf == 0)
    {
        fprintf(stderr, "Parse error\n");
        return 2;
    }

    fprintf(stderr, "w=%d; h=%d; t=%d\n", w, h, t);

    if(debug)
        printf("0x%04X, 0x%04X,", w, h);

    /* 16 bits width */
    writeWord(frle, w, order);

    /* 16 bits height */
    writeWord(frle, h, order);

    color_t* pc;
    color_t* pcstart;
    pc = (color_t*)buf;
    int i = 0;
    int cnt;
    uint16_t c0, c1, c2;
    while(i<(w*h))
    {
        /* Unpack bytes */
        cnt = 0;
        pcstart = pc;
        while(i<(w*h))
        {
            c0 = packRGB(*pc);
            c1 = packRGB(*(pc+1));
            c2 = packRGB(*(pc+2));
            if((c0 == c1) && (c1 == c2))
                break;
            if(cnt>0x7FFF) break;
            pc++;
            i++;
            cnt++;
        }

        if(cnt)
        {
            int j = 0;
            if(debug)
                printf("\n0x%04X,\n", cnt);
            /* Unpacked colors block size */
            writeWord(frle, cnt, order);
            while(cnt--)
            {
                uint16_t clr = packRGB(*pcstart);
                /*  Unpacked color */
                writeWord(frle, clr, order);

                if(debug)
                {
                    if((j & 0x07) == 0x00) printf("    ");
                    printf("0x%04X, ", clr);
                    if((j & 0x07) == 0x07) printf("\n");
                    j++;
                }
                pcstart++;
            }
        }

        /* Continuous byte */
        cnt = 0;
        pcstart = pc;
        c0 = packRGB(*pc);
        c1 = packRGB(*(pc+1));
        if(c0 == c1)
        {
            pc++;
            cnt++;
            i++;
            while(i<(w*h))
            {
                c1 = packRGB(*pc);
                if(c0 != c1) break;
                if(cnt>0x7FFF) break;
                pc++;
                cnt++;
                i++;
            }
        }

        if(cnt)
        {
            cnt |= 0x8000;
            if(debug)
                printf("\n\n0x%04X, 0x%04X,", cnt, c0);
            /* RLE packet block len */
            writeWord(frle, cnt, order);
            /* RLE color */
            writeWord(frle, c0, order);
        }
    }

    if(debug)
        printf("\n");

    if(buf)
        free(buf);

    fclose(frle);
    fclose(fxpm);

    return 0;
}

