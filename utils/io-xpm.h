#ifndef _IO_XPM_H_
#define _IO_XPM_H_

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <string.h>

typedef struct {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
} color_t;

unsigned char* loader_xpm(FILE *file, int *w, int *h, int *t);

#endif /* _IO_XPM_H_ */

