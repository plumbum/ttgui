#include <stdio.h>
#include <inttypes.h>

#include "font5x8.c"

static uint8_t reverseBits(uint8_t b)
{
    return
        ((b & 0x01)?0x80:0) |
        ((b & 0x02)?0x40:0) |
        ((b & 0x04)?0x20:0) |
        ((b & 0x08)?0x10:0) |
        ((b & 0x10)?0x08:0) |
        ((b & 0x20)?0x04:0) |
        ((b & 0x40)?0x02:0) |
        ((b & 0x80)?0x01:0);
}

int main(int argc, char** argv)
{
    printf("/*\n");
    printf(" *\n");
    printf(" * font5x8.c\n");
    printf(" *\n");
    printf(" * Most to less bits ordering\n");
    printf(" *\n");
    printf(" */\n");

    printf("const unsigned char font5x8[] = {\n");
    int i;
    for(i=0; i<sizeof(font5x8); i++)
    {
        if((i % 5) == 0)
            printf("    ");
        printf("0x%02X, ", reverseBits(font5x8[i]));
        if((i % 5) == 4)
        {
            char c = i / 5;
            if((c >= ' ') && (c < 127))
                printf("    /* '%c' */\n", c);
            else
                printf("    /* 0x%02X */\n", (uint8_t)c);
        }
            
    }
    printf("};\n");

    return 0;
}

