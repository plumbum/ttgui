#include <stdio.h>
#include <string.h>
#include <inttypes.h>

#include "font5x8.c"

int main(int argc, char** argv)
{
    printf("/*\n");
    printf(" *\n");
    printf(" * font5x8.c\n");
    printf(" *\n");
    printf(" * Left to right, top to bottom packet bits\n");
    printf(" *\n");
    printf(" */\n");

    printf("const unsigned char font5x8[] = {\n");
    int i;
    uint8_t* cc = font5x8;
    for(i=0; i<256; i++)
    {
        uint64_t buf = 0;

        uint8_t mask = 1;
        int a, b;
        for(a=0; a<8; a++)
        {
            for(b=0; b<5; b++)
            {
                buf <<= 1;
                if(cc[b] & mask) buf |= 1;
            }
            mask <<= 1;
        }
        printf("    ");
        printf("0x%02X, ", (uint8_t)((buf>>32) & 0xFF));
        printf("0x%02X, ", (uint8_t)((buf>>24) & 0xFF));
        printf("0x%02X, ", (uint8_t)((buf>>16) & 0xFF));
        printf("0x%02X, ", (uint8_t)((buf>>8) & 0xFF));
        printf("0x%02X, ", (uint8_t)((buf>>0) & 0xFF));

        if((i >= ' ') && (i < 127))
            printf("    /* '%c' */\n", i);
        else
            printf("    /* 0x%02X */\n", (uint8_t)i);

        cc += 5;
    }
    printf("};\n");

    return 0;
}

