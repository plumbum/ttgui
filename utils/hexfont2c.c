#include <inttypes.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#define INDEX_SIZE 256
int fontIndexes[INDEX_SIZE];
char fontName[256];
int fontWidth;
int fontHeight;
int fontSpace;
char fontGlyphType[256];
char fontIndexType[256];

static char szBuf[256];
static int nc;
FILE* ofile;
FILE* ifile;

int main(int argc, char** argv)
{
    int r;
    int idx;

    ofile = stdout;
    ifile = stdin;
    strcpy(fontName, "font_terminus_14b");
    fontWidth = 8;
    fontHeight = 14;
    fontSpace = 0;
    strcpy(fontGlyphType, "const uint8_t");
    strcpy(fontIndexType, "const uint16_t");


    /* TODO command line */
    

    fprintf(ofile, "/*\n * Font '%s' structures\n */\n\n", fontName);
    fprintf(ofile, "#ifndef _%s_H_\n", fontName);
    fprintf(ofile, "#define _%s_H_\n\n", fontName);
    fprintf(ofile, "#include <inttypes.h>\n\n");

    /* Describe font structure */
    fprintf(ofile, "/* Font structure type */\n");
    fprintf(ofile, "#ifndef FONT_STRUCT_T_DEFINED\n");
    fprintf(ofile, "#define FONT_STRUCT_T_DEFINED\n");
    fprintf(ofile, "typedef struct {\n");
    fprintf(ofile, "    unsigned char width;\n");
    fprintf(ofile, "    unsigned char height;\n");
    fprintf(ofile, "    unsigned char space;\n");
    fprintf(ofile, "    %s* glyphs;\n", fontGlyphType);
    fprintf(ofile, "    %s* index;\n", fontIndexType);
    fprintf(ofile, "} font_struct_t;\n");
    fprintf(ofile, "#endif\n\n");

    // fprintf(ofile, "extern const font_struct_t %s;\n\n\n", fontName);

    /* Process glyphs */
    for(idx=0; idx<INDEX_SIZE; idx++)
    {
        fontIndexes[idx] = 0;
    }
    idx = 0;
    fprintf(ofile, "static %s %s_glyphs[] = {\n", fontGlyphType, fontName);
    while((r = fscanf(ifile, "%x:%s\n", &nc, szBuf)) >= 0)
    {
        fprintf(ofile, "    ");
        char* p = szBuf;
        int size = 0;
        while(*p != 0)
        {
            char c1 = *p++;
            char c2 = *p++;
            fprintf(ofile, "0x%c%c, ", c1, c2);
            size++;
        }
        if((nc >= ' ') && (nc < 127))
            fprintf(ofile, "    /* 0x%02X '%c' */\n", (uint8_t)nc, nc);
        else
            fprintf(ofile, "    /* 0x%02X */\n", (uint8_t)nc);
        fontIndexes[nc] = idx;
        idx += size;
    }
    fprintf(ofile, "};\n\n\n");

    /* Process indexes */
    fprintf(ofile, "static %s %s_index[] = {\n", fontIndexType, fontName);
    for(idx=0; idx<INDEX_SIZE; idx++)
    {
        if((idx % 8) == 0)
            fprintf(ofile, "    ");
        fprintf(ofile, "%d, ", fontIndexes[idx]);
        if((idx % 8) == 7)
            fprintf(ofile, "\n");
    }
    fprintf(ofile, "};\n\n\n");


    /* Font structure */
    fprintf(ofile, "const font_struct_t %s = {\n", fontName);
    fprintf(ofile, "    %d, /* Width */\n", fontWidth);
    fprintf(ofile, "    %d, /* Height */\n", fontHeight);
    fprintf(ofile, "    %d, /* Space */\n", fontSpace);
    fprintf(ofile, "    %s_glyphs,\n", fontName);
    fprintf(ofile, "    %s_index\n", fontName);
    fprintf(ofile, "};\n\n\n");

    fprintf(ofile, "#endif\n\n");

    /* Tail comment */
    fprintf(ofile, "/*\n");
    time_t tt;
    struct tm* tms;
    tt = time(NULL);
    tms = localtime(&tt);
    strftime(szBuf, 256, "%F %T", tms);
    fprintf(ofile, " * Created by hexfont2c on %s\n", szBuf);
    fprintf(ofile, " * Read about hexfont2c on http://tuxotronic.org\n");
    fprintf(ofile, " */\n");

    return 0;
}

